import pandas as pd
from tqdm import tqdm
import xml.etree.ElementTree as ET
import os
import re
import text_extract
import cv2
import common_util_AZ as common_util
from keras.models import load_model


def __str_replace_xml(xml_str):
    xml_str=xml_str.replace('http://www.w3.org/1999/xhtml','')
    xml_str=xml_str.replace('❑','')
    xml_str=xml_str.replace('•','')
    xml_str=xml_str.replace('■','')
    return(xml_str)


ctr = 0

def content_extraction(p_df, xml_out_path, clf_model_path, infos_csv_path, tif_file_path):  
    c_df = pd.read_csv(infos_csv_path)
    chkbox_list = list(c_df['single_checkboxes'].values)
    circle_list = list(c_df['damage_info'].dropna().values)
    
    df = p_df
    df['filename'] = df['filename'].apply(lambda x: x.replace('.tif',''))
    
    df['xmin'] = df['xmin']*df['img_width']
    df['ymin'] = df['ymin']*df['img_height']
    df['xmax'] = df['xmax']*df['img_width']
    df['ymax'] = df['ymax']*df['img_height']
        
    df = df[['filename','xmin','ymin','xmax','ymax','label', 'img_width', 'img_height']]
    img_list = df['filename'].unique().tolist()
    group_df = df.groupby('filename')
    
    model = load_model(clf_model_path)

    temptext_df_final = pd.DataFrame()
    for img in tqdm(img_list):
        tmpdf = group_df.get_group(img)
        inner_img_list = tmpdf['filename'].unique().tolist()
        inner_group_df = tmpdf.groupby('filename')
        data_list = [] 

        for inner_img in inner_img_list:
            xml_name = os.path.join(xml_out_path, str(img.split('.')[0]) + '.xml')
            xml_str = open(xml_name, 'r', encoding = 'utf-8').read()
            upd_xml_str = __str_replace_xml(xml_str)
            xml_tree = ET.XML(upd_xml_str)
            
            image_name = os.path.join(tif_file_path, str("_".join(img.split('_')[:2])), inner_img+".jpg")
            image = cv2.imread(image_name)
            
            page_list = xml_tree.findall('.//page')
            page_height = page_list[0].attrib['height']
            c = round((df['img_height'][0])/float(page_height), 2)

            temp_df = inner_group_df.get_group(inner_img)
            ctr = 0
            
            for i, row1 in temp_df.iterrows():
                bb_xmin = int(row1.xmin)
                bb_ymin = int(row1.ymin)
                bb_xmax = int(row1.xmax)
                bb_ymax = int(row1.ymax)
                
                text_list = []
                text = " "

                if(row1.label in chkbox_list):
                    try:
                        text = common_util.detect_single_check_box(image, model, row1.xmin, row1.ymin, row1.xmax, row1.ymax)
                    except:
                        text = "Unchecked"
                    data_list.append((row1.filename, int(row1.xmin), int(row1.ymin), int(row1.xmax), int(row1.ymax), row1.label, text))
                    continue
                                    
                elif(row1.label in circle_list):
                    try:
                        text = common_util.get_damage_checked_bbox(image, row1.xmin, row1.ymin, row1.xmax, row1.ymax)
                    except:
                        text = "Unchecked"
                    data_list.append((row1.filename, int(row1.xmin), int(row1.ymin), int(row1.xmax), int(row1.ymax), row1.label, text))
                    continue

                else:
                    bb_coord=(bb_xmin,bb_ymin,bb_xmax,bb_ymax)
                    add_list = text_extract.find_text(xml_tree,bb_coord,c)
                    
                    if(len(add_list)==0):
                        text = " "
                    
                    else:
                        try:
                            if(row1.label == 'Person_Address_Details'):
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                except Exception as e:
                                    text = " "
                                    print(e)
                                    
                            elif(row1.label == 'Vehicle_VIN'):
                                try:
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = re.sub('[^A-Za-z0-9]', '', text)
                                except:
                                    text = " "
                                    
                                      
                            elif(row1.label == 'Person_Address_Details' or row1.label == 'Owner_Details' or \
                                 row1.label == 'Person_Name' or row1.label == 'Witness_Details' or row1.label == "Pass_Name_Details" or \
                                 row1.label == "Pass_Seat_SD" or row1.label == "Pass_Seat_SD"):
                                ctr += 1
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                except:
                                    text = " "
                                    
                            elif(row1.label == 'Person_Injury'):
                                ctr += 1
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = text.strip()
                                    text = text.upper()
                                    if("0" in text):
                                        text = "O"
                                    else:
                                        text = text
                                except:
                                    text = " "
                                    
                                    
                                    
                                    
                                    
                            elif(row1.label == 'Pass_DOB'):
                                ctr += 1
                                try:
                                    dob = ""
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = "".join(text_list)
                                    text = re.sub('[^0-9]', '', text)
                                    text = text.strip()
                                    
                                    if(len(text) <= 2):
                                        dob = " "
                                    elif(len(text) >= 2 and len(text) <= 6):
                                        text = text[:8]
                                        year = text[-4:]
                                        day = text[-6:-4]
                                        month = text[:2]     
                                        
                                        dob = month + "/" + day + "/" + year
                                    text = dob
                                except:
                                    text = " "
                                    
                            #Formatting error: Phone                                  
                            elif((row1.label == "Person_Telephone_Number") or (row1.label == "Vehicle_Owner_Phone_Number" or (row1.label == "Pass_Telephone"))):
                                try:
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = re.sub('[^0-9]', '', text)
                                    text = text[:10]
                                    out = text[:3] + "-" + text[3:6] + "-" + text[6:]
                                    text = out
                                except:
                                    text = " "                                    
                            
                            #Formatting error: Date
                            elif(row1.label == "Report_Date"):
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = text.replace(" ","")
                                    text = re.sub('[^0-9]', ' ', text)
                                    text = text.strip()
                                    day = text[-2:]
                                    month = text[-4:-2]
                                    year = text[:2]
                                    
                                    if(len(day) == 1):
                                        day = "0"+day
                                        
                                    if(len(month) == 1):
                                        month = "0"+month
                                    text = month + "/" + day + "/" + year
                                except:
                                    text = " "  
                                    
                                    
                            #Formatting error: Speed        
                            elif(row1.label in ["Posted_Speed_Limit"]):
                                try:
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = re.sub('[^0-9]', '', text)
                                    text = text.strip()
                                except:
                                    text = " "
                                    
                            elif(row1.label in ["Latitude"]):
                                try:
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = common_util.process_latitude(text)
                                    text = text.strip()
                                except:
                                    text = " "
                                    
                            elif(row1.label in ["Longitude"]):
                                try:
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = common_util.process_longitude(text)
                                    text = text.strip()
                                except:
                                    text = " "
                            elif(row1.label == 'Time_Invest'):
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                    text = re.sub('[^0-9]','',text)
                                    
                                    time = text[:4]
                                    if(len(time) == 4):
                                        t1 = time[:2]
                                        t2 = time[2:]
                                        out_time = t1 + ":" + t2
                                        
                                    elif(len(time) == 3):
                                        t1 = "0"+time[:1]
                                        t2 = time[1:]
                                        out_time = t1 + ":" + t2
                                        
                                    elif(len(time) == 2):
                                        t1 = time
                                        t2 = "00"
                                        out_time = t1 + ":" + t2
                                    else:
                                        out_time = " "
                                        
                                    text = out_time
                                    
                                except:
                                    text = " "
                                    print("exception in --> Time_Notified")
    
                            elif(row1.label == 'Vehicle_Make' or row1.label == 'Vehicle_Owner' or row1.label == "Person_City_State_ZIP" or \
                                 row1.label == "Damage_Property_Owner_Name" or row1.label == "Damage_Property_Owner_Address" or row1.label == "Owner_Details"):
                                try:
                                    add_list = sorted(add_list, key = lambda x: x[0])
                                    text_list = [x[2] for x in add_list]
                                    text = " ".join(text_list)
                                except:
                                    text = " "
                                    print("exception in --> Vehicle_Make")
                                    
                            else:
                                text_list = [x[2] for x in add_list]
                                text = " ".join(text_list)
                                
                                text = re.sub(r'^[\W\.\_]*', '', text)
                                text = re.sub(r'\W*$', '', text)
                                if((re.match("[a-zA-Z0-9]*", text)) ==None):
                                    text = ''
                                text = re.sub(r"(\d+)\s(-)\s(\d+)", r"\1\2\3", text.rstrip())
                                text = re.sub(r"(\d+)(-)\s(\d+)", r"\1\2\3", text.rstrip())
                                text = re.sub(r"(\d+)\s(-)(\d+)", r"\1\2\3", text.rstrip())
                                text = re.sub(r"(\d+)\s(\d+)", r"\1\2", text.rstrip())
                                char_check_end = re.compile('[(]')
                                if(char_check_end.search(text)!= None):
                                    text = text+')'
                                char_check_beg = re.compile('[)]')
                                if(char_check_beg.search(text)!= None):
                                    if(char_check_end.search(text) ==None):
                                        text = '('+text
    
                                if(text =='I'):
                                    text = '1'
                            
                            text = re.sub(' +', ' ',text).strip()
    
                        except:
                            text = ' '
                    
                    if(text.strip()==""):
                        text = " "
                    data_list.append((row1.filename, int(row1.xmin), int(row1.ymin), int(row1.xmax), int(row1.ymax), row1.label, text))
            temptext_df = pd.DataFrame(data_list, columns = ['path', 'xmin', 'ymin', 'xmax', 'ymax', 'label', 'text'])
            temptext_df['page_no'] = temptext_df['path'].apply(lambda x: 'page_no_' +x[-1])
            temptext_df.sort_values(by = ['path'], inplace = True, ascending = True)
            temptext_df_final = temptext_df_final.append(temptext_df, ignore_index = True)
            # temptext_df_final = desc_to_code(temptext_df_final, code_description_lookup_path)
            
#    temptext_df_final_code = desc_to_code(temptext_df_final, code_description_lookup_path)    
    return temptext_df_final
