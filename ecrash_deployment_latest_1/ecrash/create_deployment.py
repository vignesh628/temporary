import ecrash_main
import subprocess
version = ecrash_main.__version__

image_name = "spianalytics/ecrash_main"
print("--------------------building docker image with the following name: {}:{}------------------------------".format(image_name, version))
subprocess.call(["docker", "build", "--no-cache", "-t", "{}:{}".format(image_name, version), "."])

print("--------------------Manually push the image to docker hub! Remember to sign in to dockerhub before you push!-----------------------")
