import numpy as np

def update_coord(coord,c):
    coord = [float(i)*c for i in coord]
    return(coord)

def intersection(ocr_coord,bb_coord):
    [y_min1, x_min1, y_max1, x_max1] = np.split(ocr_coord, 4, axis=1)
    [y_min2, x_min2, y_max2, x_max2] = np.split(bb_coord, 4, axis=1)

    all_pairs_min_ymax = np.minimum(y_max1, np.transpose(y_max2))
    all_pairs_max_ymin = np.maximum(y_min1, np.transpose(y_min2))
    intersect_heights = np.maximum(
      np.zeros(all_pairs_max_ymin.shape),
      all_pairs_min_ymax - all_pairs_max_ymin)
    all_pairs_min_xmax = np.minimum(x_max1, np.transpose(x_max2))
    all_pairs_max_xmin = np.maximum(x_min1, np.transpose(x_min2))
    intersect_widths = np.maximum(
      np.zeros(all_pairs_max_xmin.shape),
      all_pairs_min_xmax - all_pairs_max_xmin)
    return intersect_heights * intersect_widths

def area(ocr_coord):
    return (ocr_coord[:, 2] - ocr_coord[:, 0]) * (ocr_coord[:, 3] - ocr_coord[:, 1])


def check_text_in_BB(ocr_coord,bb_coord,ocr_text):
    ocr_coord = np.expand_dims(np.array(ocr_coord),axis=0)
    bb_coord = np.expand_dims(np.array(bb_coord),axis=0)
    intersect = intersection(ocr_coord,bb_coord)
    text_area = area(ocr_coord)
    intersect_over_text_area = intersect/text_area
    if (intersect_over_text_area>0.5):
        result_text = ocr_text
    else:
        result_text = ''
    return(result_text)


def find_text(xml_tree,bb_coord,c):
    add_list = []
    text_list = []
    for elem in xml_tree.findall('.//page/flow/block/line/word'):
        coord=(elem.attrib['xMin'],elem.attrib['yMin'],elem.attrib['xMax'],elem.attrib['yMax'])
        ocr_coord=update_coord(coord,c)
        result_text = check_text_in_BB(ocr_coord, bb_coord, elem.text)
        if(result_text is not None and result_text!=''):
            add_list.append([float(elem.attrib['xMin']),float(elem.attrib['xMax']),result_text]) 
    return(add_list)