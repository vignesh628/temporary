import os
import warnings
from .about import __version__

if os.environ.get("ECRASH_LOG_DIR", None) is None:
    warnings.warn("ECRASH_LOG_DIR environment variable is not set! Will throw environment error when trying to import the engine.py. You can ignore this error if you are just creating the docker image")
else:
    from .engine import ECrashEngine
