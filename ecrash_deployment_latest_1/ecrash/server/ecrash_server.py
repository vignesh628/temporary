import logging
import os
import flask
import json
from time import time
from ecrash_main import ECrashEngine
from ecrash_main.errors import ModelNotImplementedError, TensorflowServingError
from ecrash_main import __version__ as version

app = flask.Flask(__name__)

# Logger Config
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
filehandler = logging.FileHandler(os.path.join(os.environ["ECRASH_LOG_DIR"], "ecrash_flask_app_v{}.log".format(version)))
filehandler.setFormatter(formatter)

logger.addHandler(filehandler)

# setup the Embase Engine
config_file = "/home/ecrash_user/ecrash_deployment/server/config.json"  # path inside docker container

with open(config_file, "r") as f:
    loaded_config = json.load(f)
logger.info("Initializing the E-Crash Main engine...")
engine = ECrashEngine(loaded_config)
logger.info("E-Crash Server is ready!")

# -------------------------------------------------------------------------------- #


@app.route("/")
@app.route("/home")
def home():
    return "E-Crash Engine. use /predict for POST requests. The payload should be {'tiff':tiff, 'filename':filename, 'model_name':model_name}"


@app.route("/predict", methods=["POST"])
def predict():
    data = {"status": 500}
    if flask.request.method == "POST":
        st_time = time()
        try:
            tiff = flask.request.files["tiff"].read()
            filename = flask.request.files["filename"].read().decode("utf-8")
            model_name = flask.request.files["model_name"].read().decode("utf-8")
        except Exception:
            message = "Error in Input."
            logger.exception(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        if not isinstance(filename, str):
            message = "filename must be a string but got {} for file {}".format(type(filename), filename)
            logger.error(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        if not isinstance(model_name, str):
            message = "model_name must be a string but got {} for file {}".format(type(model_name), model_name)
            logger.error(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        if not isinstance(tiff, bytes):
            message = "tiff must be a byte object but got {} for file {}".format(type(tiff), filename)
            logger.error(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        logger.info("time taken to get data for filename {} is {}. Using model {} for prediction".format(filename, time()-st_time, model_name))
        st_time = time()
        try:
            list_of_dataframes = engine.predict(tiff=tiff, tiff_filename=filename, model_name=model_name)

        except ModelNotImplementedError:
            message = "Model {} is not implemented error for file {}. See logs for details".format(model_name, filename)
            logger.exception(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        except TensorflowServingError:
            message = "Tensorflow serving error for file {}. See logs for details".format(filename)
            logger.exception(message)
            data["status"] = 500
            data["exception"] = message
            return flask.jsonify(data)

        except Exception as e:
            message = "Unknown error: {} for file {}. See logs for details".format(e, filename)
            logger.exception(message)
            data["status"] = 500
            data["exception"] = message
            return flask.jsonify(data)

        # convert the list of dataframes to JSON
        json_data = {}
        for i, temp_df in enumerate(list_of_dataframes):
            json_data[i] = json.loads(temp_df.to_json(orient="records"))
        logger.info("Time taken for predicting filename {} is {}".format(filename, time()-st_time))
        data["status"] = 200
        data["data"] = json_data

    return flask.jsonify(data)
