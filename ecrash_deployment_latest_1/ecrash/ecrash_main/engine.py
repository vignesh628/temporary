import logging
import os
import pickle
import numpy as np

from pdf2image import convert_from_bytes
from time import time
import io
from PIL import Image, ImageSequence

from .tf_serving_utils import get_prediction_df
from .errors import ModelNotImplementedError, TensorflowServingError
from .about import __version__ as version

# Logger Config
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
filehandler = logging.FileHandler(os.path.join(os.environ["ECRASH_LOG_DIR"], "ecrash_engine_v{}.log".format(version)))
filehandler.setFormatter(formatter)

logger.addHandler(filehandler)


class ECrashEngine:
    def __init__(self, config):
        logger.info("Initializing the engine")
        self.config = config
        self.model_labelmaps = {}
        for model_name in self.config["models"].keys():
            with open(self.config["models"][model_name]["labelmap_path"], "rb") as f:
                self.model_labelmaps[str(model_name)] = pickle.load(f)

        logger.info("Found and loaded {} labelmaps corresponding to: {}".format(len(self.model_labelmaps), self.model_labelmaps.keys()))
        logger.info("Engine is ready for predictions")

    def _predict_on_single_image(self, pil_image, image_name):
        st_time = time()
        image = np.array(pil_image)
        if len(image.shape) == 2:
            logger.warn("Got a gray scale image for tiff {} with image name {}. \
             Stacking it 3 times to get RGB channels".format(self.tiff_filename, image_name)
                        )
            image = np.stack((image,)*3, axis=2)
        assert len(image.shape) == 3, "image must be a 3 channel image. But got {} for tiff {} with image name {}".format(len(image.shape), self.tiff_filename, image_name)
        try:
            prediction_df = get_prediction_df(image=image,
                                              server_ip=self.config["tf_serving_ip"],
                                              port=self.config["tf_serving_port"],
                                              label_map=self.model_labelmaps[self.model_name],
                                              model_name=self.required_model_config["model_name"],
                                              signature_name=self.required_model_config["model_signature"],
                                              input_name=self.required_model_config["model_input_name"],
                                              filename=image_name,
                                              timeout=self.config["timeout"],
                                              score_thresh=self.required_model_config["score_threshold"],
                                              overlap_threshold=self.required_model_config.get("overlap_threshold", None),
                                              normalize=bool(self.required_model_config["normalize"])
                                              )
        except Exception:
            logger.exception("Error while calling tensorflow serving for image {} part of tiff {}".format(image_name, self.tiff_filename))
            raise TensorflowServingError

        logger.info("Time taken for image {} part of {} tiff is {}".format(image_name, self.tiff_filename, time()-st_time))
        return prediction_df

    def predict(self, tiff, tiff_filename, model_name):
        """Creates predictions for the given tiff

        Arguments:
            tiff (bytes): tiff file as bytes. No Default
            tiff_filename (str): filename of tiff. No Default
            model_name (str): model name to use for prediction. No Default

        Returns:
            A list of DataFrames
        """
        self.tiff_filename = tiff_filename
        self.model_name = model_name
        try:
            self.required_model_config = self.config["models"][model_name]
        except Exception:
            logger.exception("the given model {} is not implemented for tiff {}".format(model_name, tiff_filename))
            raise ModelNotImplementedError
        try:
            self.model_labelmaps[model_name]
        except KeyError:
            logger.exception("the given model {} is not implemented for tiff {}".format(model_name, tiff_filename))
            raise ModelNotImplementedError
        st_time = time()
        im = Image.open(io.BytesIO(tiff))
        # no_of_frames = im.n_frames
        pil_images = []
        for j, page in enumerate(ImageSequence.Iterator(im)):
            RGB_img = page.convert("RGB")
            pil_images.append(RGB_img)
        # pil_images = convert_from_bytes(tiff)
        logger.info("Time taken to convert the given tiff {} to images is {}. The tiff contains {} pages".format(self.tiff_filename, time()-st_time, len(pil_images)))
        image_names = ["{}_{}".format(tiff_filename, i+1) for i in range(len(pil_images))]
        predictions = [self._predict_on_single_image(pil_image, image_name) for pil_image, image_name in zip(pil_images, image_names)]

        return predictions
