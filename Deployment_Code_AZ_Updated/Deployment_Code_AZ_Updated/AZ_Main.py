import os
import pandas as pd
import sys
import ARIZONA.AZ_text_extractor as AZ_text_extractor
import ARIZONA.AZ_json_conversion as az_json


city_lookup_path = os.path.join(os.getcwd(), "ARIZONA", "CityList.txt")
business_name_lookup_path = os.path.join(os.getcwd(), "ARIZONA", "Business_Name.csv")
infos_csv_path = os.path.join(os.getcwd(), "ARIZONA", "driver_infos_list.csv")
mapping_df_path = os.path.join(os.getcwd(), "ARIZONA", "AZ_16_Element_List.csv")
clf_model_path = os.path.join(os.getcwd(), "ARIZONA", "checkbox.h5")
tif_file_path = os.path.join(os.getcwd(), "ARIZONA", "temp_jpg")
temp_path = os.path.join(os.getcwd(), "ARIZONA", "temp_xml")


def Process(csv_file_path, temp_path, clf_model_path):
    csv_file_name = str(os.path.basename(csv_file_path).split('.')[0])
    predicted_df = pd.read_csv(csv_file_path)
    text_extracted_df = pd.DataFrame()

    try:
        print("Process : PDF Content Extraction & Post-Processing Layer")
        
        text_extracted_df = AZ_text_extractor.content_extraction(predicted_df, os.path.join(temp_path, csv_file_name), clf_model_path, infos_csv_path, tif_file_path)
    except Exception as e:
        print("Error : PDF Content Extraction & Post-Processing Layer")
        return sys.exc_info(), "error"

    try:
        print("Process : JSON Conversion")
        data, tif_name = az_json.extract_json_arizona(text_extracted_df, infos_csv_path, city_lookup_path, business_name_lookup_path, mapping_df_path)
        return data, tif_name
    except:
        print("Error : Error occured during JSON Conversion")
        return sys.exc_info(), "error"