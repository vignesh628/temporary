from typing import List
import json
import pandas as pd
from fuzzywuzzy import fuzz
global num_property_owner
global party_number
from common_util_AZ import *
from statistics import mode
import math
import re

def get_label_count(df, label):
    list_ = list(df['label'].values)
    count = list_.count(label)
    return count

def get_person_type(dic):
    party_is_driver = get_value_from_dict(dic,'Person_Driver_Check')
    party_is_driverless = get_value_from_dict(dic,'Person_Driverless_Check')
    party_is_pedestrian = get_value_from_dict(dic,'Person_Pedestrian_Check')
    party_is_pedacyclist = get_value_from_dict(dic,'Person_Pedacycle_Check')


    if(party_is_driver == "Checked"):
        p_type = "DRIVER"
    elif(party_is_driverless == "Checked"):
        p_type = "DRIVERLESS"
    elif(party_is_pedestrian == "Checked"):
        p_type = "PEDESTRIAN"
    elif(party_is_pedacyclist == "Checked"):
        p_type = "PEDACYCLIST"
    else:
        p_type = "DRIVER"

    return p_type

def name_split_in_given_order(name_order, name_match, add_dict):
    if name_order == 'FML':
        if len(name_match.groups()) == 3:
            add_dict.update({'first_name': name_match.group(2).upper()})
            add_dict.update({'last_name': name_match.group(3).upper()})
        elif len(name_match.groups()) == 4:
            add_dict.update({'first_name': name_match.group(2).upper()})
            add_dict.update({'middle_name': name_match.group(3).upper()})
            add_dict.update({'last_name': name_match.group(4).upper()})
        elif len(name_match.groups()) == 5:
            add_dict.update({'first_name': name_match.group(2).upper()})
            add_dict.update({'middle_name': name_match.group(3).upper()})
            add_dict.update({'last_name': name_match.group(4).upper() + ' ' + name_match.group(5).upper()})
    if name_order == 'LFM':
        if len(name_match.groups()) == 3:
            add_dict.update({'first_name': name_match.group(3).upper()})
            add_dict.update({'last_name': name_match.group(2).upper()})
        elif len(name_match.groups()) == 4:
            add_dict.update({'first_name': name_match.group(3).upper()})
            add_dict.update({'middle_name': name_match.group(4).upper()})
            add_dict.update({'last_name': name_match.group(2).upper()})
        elif len(name_match.groups()) == 5:
            add_dict.update({'first_name': name_match.group(3).upper()})
            add_dict.update({'middle_name': name_match.group(4).upper() + ' ' + name_match.group(5).upper()})
            add_dict.update({'last_name': name_match.group(2).upper()})
 
         
def name_split_using_pattern(name_text, name_order='', ischecked = False, business_name_lookup_path=""):
    add_dict = {'first_name': '', 'middle_name': '', 'last_name': '', 'suffix':'', 'name': ''}
    name_text = name_text.strip()

    # =====================================
    # Business name - logic
    if ischecked == True:
        add_dict.update({'last_name': name_text.upper()})
        add_dict.update({'name': name_text.upper()})
        return add_dict
    with open(business_name_lookup_path) as f:
        business_name_lookup = f.readlines()
        business_name_lookup = [x.strip() for x in business_name_lookup]
    business_names = '|'.join(business_name_lookup)
    business_names = business_names.replace(' ', '\s')
    business_name_match = re.search('\\b(Holdings|LLC|INC|CORP|COMPANY|ENVIROMENTAL|HYUNDAI|TRANSPORTATION|GROUP|CO|INCORPORATED)\\b',name_text, re.IGNORECASE)

    
    
    if business_name_match:
        add_dict.update({'last_name': name_text.upper()})
        add_dict.update({'name': name_text.upper()})
        return add_dict
    # =====================================

    suffix_list = ['JR', 'BVM', 'CFRE', 'CLU', 'CPA', 'CSC', 'CSJ', 'DC', 'DD', 'DDS', 'DMD', 'DO', 'DVM', 'EDD',
                   'ESQ', 'II', 'III', 'IV', 'INC', 'JD', 'LLD', 'LTD', 'MD', 'OD', 'OSB', 'PC', 'PE', 'PHD',
                   'RET', 'RGS', 'RN', 'RNC', 'SHCJ', 'SJ', 'SNJM', 'SR', 'SSMO', 'USA', 'USAF', 'USAFR', 'USAR',
                   'USCG', 'USMC', 'USMCR', 'OR']
    suffix_found = ''
    for suffix in suffix_list:
        suffix_match = re.search(r'\b' + suffix + r'\b', name_text, re.IGNORECASE)
        if suffix_match:
            suffix_found = suffix_match.group(0)
            name_text = re.sub('(\s)' + suffix_match.group(0), '', name_text, re.IGNORECASE)
            name_text = re.sub(suffix_match.group(0) + '(\s)', '', name_text, re.IGNORECASE)
            break

    name_match_1 = re.search("^(([A-Za-z]+)\s+([A-Za-z]+))$", name_text)  # Priyanga Jaanagi
    name_match_2 = re.search("^(([A-Za-z]+)\s+([A-Za-z]+)\s+([A-Za-z]+))$", name_text)  # Priyanga Jaanagi Vaithinaden
    name_match_3 = re.search("^(([A-Za-z]+)\s+([A-Za-z]+)\s+([A-Za-z]+)\s+([A-Za-z]+))$", name_text)  # Priyanga Jaanagi Vaithinaden Vaithi
    name_match_4 = re.search("^(([A-Za-z]+)\s*,\s*([A-Za-z]+)\s+([A-Za-z]+))$", name_text)  # Priyanga, Jaanagi Vaithinaden
    name_match_5 = re.search("^(([A-Za-z]+)\s*,\s*([A-Za-z]+)\s*,\s*([A-Za-z]+))$", name_text)  # Priyanga, Jaanagi, Vaithinaden
    name_match_6 = re.search("^(([A-Za-z]+\s+[A-Za-z]+)\s*,\s*([A-Za-z]+)\s+([A-Za-z]+\s+[A-Za-z]+))$", name_text)
    name_match_7 = re.search("^(([A-Za-z]+\s+[A-Za-z]+)\s*,\s*([A-Za-z]+))$", name_text)  # Priyanga Jaanagi, Vaithinaden
    name_match_8 = re.search("^(([A-Za-z]+)\s*,\s*([A-Za-z]+))$", name_text)  # Priyanga, Jaanagi
    name_match_9 = re.search("^(([A-Za-z]+),\s*([A-Za-z]+)\s+([A-Z]\s?\.\s?[A-Za-z]+))$", name_text)  # Solis, Jose M.Morales
    name_match_10 = re.search("^(([A-Za-z]{3,})\s+([A-Za-z]{3,})\s+([A-Za-z]{3,})\s+([A-Za-z]{1}))$", name_text)  # SLub Rodrigueza Torres A
    name_match_11 = re.search("^(([A-Za-z]{3,}\s*-\s*[A-Za-z]{3,})\s+([A-Za-z]{3,}))$", name_text)  # Galeana-Garica Gabriela
    name_match_12 = re.search("^(([A-Za-z]+\s+[A-Za-z]+)\s*,\s*([A-Za-z]+)\s+([A-Za-z]+))$", name_text)  # Van Lue, Tyler A
    name_match_13 = re.search("^(([A-Za-z]+)\s*\,\s*([A-Za-z]+\s+[A-Za-z]+)\s*\,\s*([A-Za-z]+))$", name_text)  # RAMIREZ, J OANN, MARIE
    
    if name_match_1:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_1, add_dict)
        else:
            add_dict.update({'first_name': name_match_1.group(2).upper()})
            add_dict.update({'last_name': name_match_1.group(3).upper()})
    elif name_match_2:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_2, add_dict)
        else:
            add_dict.update({'first_name': name_match_2.group(2).upper()})
            add_dict.update({'middle_name': name_match_2.group(3).upper()})
            add_dict.update({'last_name': name_match_2.group(4).upper()})
    elif name_match_10:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_10, add_dict)
        else:
            add_dict.update({'first_name': name_match_10.group(2).upper()})
            add_dict.update({'middle_name': name_match_10.group(3).upper() + ' ' + name_match_10.group(5).upper()})
            add_dict.update({'last_name': name_match_10.group(4).upper()})
    elif name_match_3:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_3, add_dict)
        else:
            add_dict.update({'first_name': name_match_3.group(2).upper()})
            add_dict.update({'middle_name': name_match_3.group(3).upper()})
            add_dict.update({'last_name': name_match_3.group(4).upper() + ' ' + name_match_3.group(5).upper()})
    elif name_match_4:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_4, add_dict)
        else:
            add_dict.update({'first_name': name_match_4.group(3).upper()})
            add_dict.update({'middle_name': name_match_4.group(4).upper()})
            add_dict.update({'last_name': name_match_4.group(2).upper()})
    elif name_match_5:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_5, add_dict)
        else:
            add_dict.update({'first_name': name_match_5.group(2).upper()})
            add_dict.update({'middle_name': name_match_5.group(3).upper()})
            add_dict.update({'last_name': name_match_5.group(4).upper()})
    elif name_match_6:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_6, add_dict)
        else:
            add_dict.update({'first_name': name_match_6.group(3).upper()})
            add_dict.update({'middle_name': name_match_6.group(4).upper()})
            add_dict.update({'last_name': name_match_6.group(2).upper()})
    elif name_match_7:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_7, add_dict)
        else:
            add_dict.update({'first_name': name_match_7.group(3).upper()})
            add_dict.update({'last_name': name_match_7.group(2).upper()})
    elif name_match_8:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_8, add_dict)
        else:
            add_dict.update({'first_name': name_match_8.group(3).upper()})
            add_dict.update({'last_name': name_match_8.group(2).upper()})
    elif name_match_9:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_9, add_dict)
        else:
            add_dict.update({'first_name': name_match_9.group(3).upper()})
            add_dict.update({'middle_name': name_match_9.group(4).upper()})
            add_dict.update({'last_name': name_match_9.group(2).upper()})
    elif name_match_11:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_11, add_dict)
        else:
            add_dict.update({'first_name': name_match_11.group(3).upper()})
            add_dict.update({'last_name': name_match_11.group(2).upper()})
    elif name_match_12:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_12, add_dict)
        else:
            add_dict.update({'first_name': name_match_12.group(3).upper()})
            add_dict.update({'middle_name': name_match_12.group(4).upper()})
            add_dict.update({'last_name': name_match_12.group(2).upper()})
    elif name_match_13:
        if name_order != '':
            name_split_in_given_order(name_order, name_match_13, add_dict)
        else:
            add_dict.update({'first_name': name_match_13.group(2).upper()})
            add_dict.update({'middle_name': name_match_13.group(3).upper()})
            add_dict.update({'last_name': name_match_13.group(4).upper()})
    else:
        add_dict.update({'last_name': name_text})

    add_dict.update({'suffix': suffix_found.upper()})
    add_dict.update({'name': name_text.upper()})

    return add_dict






def get_value_from_dict(dict_name,label_name):
    if(label_name in dict_name.keys()):
        value = str(dict_name[label_name])
    else:
        value = ''
    return value
        
def get_driver_details_in_each_segment(driver_list,key_name,driver_no):
    try:
        for i in range(len(driver_list)):
            vars()["driver_{}".format(i+1)] = driver_list[i]
        val = vars()["driver_{}".format(driver_no)][key_name]
    except:
        val =  ""
        
    return val.strip()

def get_passenger_details_in_each_segment(passenger_list,key_name,passenger_no):
    try:
        for i in range(len(passenger_list)):
            vars()["passenger_{}".format(i+1)] = passenger_list[i]
        val = vars()["passenger_{}".format(passenger_no)][key_name]
    except:
        val =  ""

    return val

def count_blank_in_dic(dic):
    count = 0
    for val in dic.values():
        if(val.strip() == '' or val.strip() == ' '):
            count+= 1

    return count


def get_citation_params(df, citation_infos_list):        
    label_count_dict = dict()

    list_of_labels = citation_infos_list
    
    for label in list_of_labels:
        label_count_dict[label] = get_label_count(df, label)
    
    try:
        all_citation_count = mode(list(label_count_dict.values()))
    except:
        all_citation_count = max(list(label_count_dict.values()))
        
    for i in range(all_citation_count):
        globals()["citation_{}".format(i+1)] = dict()
        
    total_pages = df['page_no'].nunique()
    page_count = 1
    for page_no in range(total_pages):
        page_label_count_dict = dict()
        group_df = df.groupby('page_no')
        try:
            page_df = group_df.get_group('page_no_{}'.format(page_no+1))
        except:
            continue
        
        for label in list_of_labels:
            page_label_count_dict[label] = get_label_count(page_df, label)
            
        try:
            page_citation_count = mode(list(page_label_count_dict.values()))
        except:
            page_citation_count = max(list(page_label_count_dict.values()))

        if(page_citation_count != 0):
            if(page_label_count_dict['Citation_Code'] == page_citation_count):
                check_label = 'Citation_Code'
                
            elif(page_label_count_dict['Citation_Unit_No'] == page_citation_count):
                check_label = 'Citation_Unit_No'
        
            else:
                idx = list(page_label_count_dict.values()).index(page_citation_count)
                check_label = list(page_label_count_dict.keys())[idx]
    
            check_label_ymin = []
    
            for i,row in page_df.iterrows():
                if(row.label == check_label):
                    check_label_ymin.append(row.ymin)
                else:
                    continue
    
            check_label_ymin = sorted(check_label_ymin)
    
            for i in range(len(check_label_ymin)):
                globals()["yminss_{}".format(i+1)] = check_label_ymin[i]
    
    
    
            for i, row in page_df.iterrows():
                
                if(row.label in citation_infos_list):
                    label = row.label
    
                    y_min = row.ymin
                    x_min = row.xmin
    
                    for a in range(len(check_label_ymin)):
                        if(a !=  len(check_label_ymin)-1 and y_min >=  (globals()["yminss_{}".format(a+1)]-1) and y_min <=  (globals()["yminss_{}".format(a+2)]-1)):
                            globals()['citation_{}'.format(a+1)][label] = row.text
    
                        elif(a == len(check_label_ymin)-1):
                            globals()['citation_{}'.format(a+1)][label] = row.text
        else:
            continue

    citation_list = [globals()['citation_{}'.format(i+1)] for i in range(all_citation_count)]
    return citation_list, all_citation_count   




 

def name_address_split_using_lookup(address_text, city_lookup_path):

    add_dict = {'names': '', 'address': '', 'city': '', 'state': '', 'zipcode': ''}
    state_list = ['CA', 'NC', 'TX', 'NJ', 'FL', 'NY', 'IL', 'PA', 'OH', 'NM', 'GA', 'MD', 'MI',
                  'LA', 'MN', 'WI', 'KS', 'OK', 'IA', 'NV', 'UT', 'MS', 'OR', 'KY', 'TN', 'NH',
                  'HI', 'NE', 'WV', 'DE', 'ID', 'IN', 'DC', 'SD', 'RI', 'AK', 'VT', 'ME', 'WY',
                  'ND', 'PR', 'BC', 'VI', 'GU', 'ON', 'PQ', 'AB', 'NS', 'YT', 'MB', 'NF', 'WA', 'AZ',
                  'Florida', 'Texas', 'California', 'New York', 'Pennsylvania', 'Ohio',
                  'Tennessee', 'Wisconsin', 'Missouri', 'Michigan', 'New Jersey', 'Georgia',
                  'Minnesota', 'Mississippi', 'Louisiana', 'Alabama', 'Maryland', 'Connecticut',
                  'Oklahoma', 'Oregon', 'Arkansas', 'Massachusetts', 'West Virginia', 'Iowa', 'New Mexico',
                  'Nebraska', 'Virginia', 'Delaware', 'New Hampshire', 'Arizona', 'Washington', 'Kansas',
                  'Montana', 'Maine', 'Nevada', 'Kentucky', 'Vermont', 'Rhode Island', 'Hawaii', 'Colorado',
                  'Dis Of Columbia', 'Alaska', 'Wyoming', 'North Dakota', 'South Carolina', 'North Carolina',
                  'Illinois', 'Idaho', 'Virgin Island', 'Indiana', 'South Dakota', 'Guam', 'New Brunswick',
                  'Puerto Rico', 'Washington'
                  'Yukon', 'Utah', 'British Columbia']

    city = ''
    state = ''
    zipcode = ''
    names = ''
    phone_number = ''
    
    
    try:
        phone_number = re.findall('(\d{3}[-\.\s]\d{3}[-\.\s]\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]\d{4}|\d{3}[-\.\s]\d{4})', address_text)[0]
    except:
        phone_number = ""
    address_text = address_text.replace(phone_number, '')
    address_text = re.sub('[^A-Za-z0-9 ]', '', address_text)
    

    with open(city_lookup_path) as f:
        city_lookup = f.readlines()
        city_lookup = [x.strip() for x in city_lookup]
    states = '|'.join(state_list)
    cities = '|'.join(city_lookup)
    cities = cities.replace(' ', '\s')

    name_match = re.search('^(([A-Za-z\,?\s&-\.\/]+)(\,|\s))', address_text)
    if name_match:
        names = name_match.group(2)
        address_text = address_text.replace(name_match.group(1), '')
        address_text = address_text.strip()

    zipcode_match = re.search('((\s|,)([0-9]{5,})(\s|,)?)$', address_text, re.IGNORECASE)
    if zipcode_match:
        zipcode = zipcode_match.group(3)
        address_text = address_text.replace(zipcode_match.group(1), '')
        address_text = address_text.strip()
        

    state_match = re.search('((\s|,)(' + states + ')(\s|,)?)$', address_text, re.IGNORECASE)
    if state_match:
        state = state_match.group(3)
        address_text = address_text.replace(state_match.group(1), '')
        address_text = address_text.strip()

    city_match = re.search('((\s|,)(' + cities + ')(\s|,)?)$', address_text, re.IGNORECASE)
    if city_match:
        city = city_match.group(3)
        address_text = address_text.replace(city_match.group(1), '')
        address_text = address_text.strip()
        
    # phone_number_match = re.search(r'([0-9\-]+)', address_text, re.IGNORECASE)
    # if phone_number_match:
    #     phone_number = phone_number_match.group(4)
    #     address_text = address_text.replace(phone_number_match.group(1), '')        
        
    address = address_text
    address = address.strip(', ')

    add_dict.update({'names': names})
    add_dict.update({'address': address})
    add_dict.update({'city': city})
    add_dict.update({'state': state})
    add_dict.update({'zipcode': zipcode})
    add_dict.update({'phone': phone_number})

    return add_dict 

        
def get_violation_code(text):
    dic = dict()
    dic["Code"] = text
    dic["Description"] = ""
    return [dic]          

def get_value_from_df(df,label_name):
    if len(df.loc[df['label'] == label_name]) > 0:
        value = df.loc[df['label'] == label_name]['text'].iloc[0]
        if str(value) != 'nan':
            return value.strip()
        else:
            return ''
    else:
        return ''
    
def get_dic_value(dic, label):
    try:
        value = dic[label]
    except:
        value = ""
    return value

def get_passenger_params(df, passenger_infos_list):        
    label_count_dict = dict()

    list_of_labels = passenger_infos_list
    
    for label in list_of_labels:
        label_count_dict[label] = get_label_count(df, label)
        
    try:
        all_passenger_count = mode(list(label_count_dict.values()))
    except:
        all_passenger_count = max(list(label_count_dict.values()))


    for i in range(all_passenger_count):
        globals()["passenger_{}".format(i+1)] = dict()
        
    total_pages = df['page_no'].nunique()
    page_count = 1
    for page_no in range(total_pages):
        page_label_count_dict = dict()
        group_df = df.groupby('page_no')
        try:
            page_df = group_df.get_group('page_no_{}'.format(page_no+1))
        except:
            continue

        for label in list_of_labels:
            page_label_count_dict[label] = get_label_count(page_df, label)

        try:
            page_passenger_count = mode(list(page_label_count_dict.values()))
        except:
            page_passenger_count = max(list(page_label_count_dict.values()))

        if(page_passenger_count != 0):
            if(page_label_count_dict['Pass_Unit_No'] == page_passenger_count):
                check_label = 'Pass_Unit_No'
                
            elif(page_label_count_dict['Pass_Seat_Pos'] == page_passenger_count):
                check_label = 'Pass_Seat_Pos'
    
            elif(page_label_count_dict['Pass_Seat_SD'] == page_passenger_count):
                check_label = 'Pass_Seat_SD'
    
            elif(page_label_count_dict['Pass_Seat_IS'] == page_passenger_count):
                check_label = 'Pass_Seat_IS'
    
            elif(page_label_count_dict['Pass_Telephone'] == page_passenger_count):
                check_label = 'Pass_Telephone'
    
            elif(page_label_count_dict['Pass_DOB'] == page_passenger_count):
                check_label = 'Pass_DOB'
                
            elif(page_label_count_dict['Pass_Sex'] == page_passenger_count):
                check_label = 'Pass_Sex'  
                                
    
            else:
                idx = list(page_label_count_dict.values()).index(page_passenger_count)
                check_label = list(page_label_count_dict.keys())[idx]
    
            check_label_ymin = []
    
            for i,row in page_df.iterrows():
                if(row.label == check_label):
                    check_label_ymin.append(row.ymin)
                else:
                    continue
    
            check_label_ymin = sorted(check_label_ymin)
    
            for i in range(len(check_label_ymin)):
                globals()["ymin_{}".format(i+1)] = check_label_ymin[i]
    
            factor = (page_passenger_count*page_count) - page_passenger_count
    
    
            for i, row in page_df.iterrows():
                
                if(row.label in passenger_infos_list):
                    label = row.label
    
                    y_min = row.ymin
    
                    for a in range(len(check_label_ymin)):
                        if(a !=  len(check_label_ymin)-1 and y_min >=  (globals()["ymin_{}".format(a+1)]-1) and y_min <=  (globals()["ymin_{}".format(a+2)]+1)):
                            globals()['passenger_{}'.format(a+1+factor)][label] = row.text
    
                        elif(a == len(check_label_ymin)-1):
                            globals()['passenger_{}'.format(a+1+factor)][label] = row.text

             
        else:
            continue

    passenger_list = [globals()['passenger_{}'.format(i+1)] for i in range(all_passenger_count)]
    return passenger_list, all_passenger_count




        

            
            
#ARIZONA STARTS HETRE   

class Incident(object):
    def __init__(self, Case_Identifier:str, Crash_City:str, Crash_Date:str, Dispatch_Time:str, Gps_Other:str,
                  Latitude:str, Longitude:str, Loss_Cross_Street:str, Loss_State_Abbr:str, Loss_Street:str, Photographs_Taken:str, 
                  Report_Type_Id:str, Weather_Condition:str, Incident_Hit_and_Run:str, **kwargs):
        
        self.Case_Identifier = Case_Identifier
        self.Crash_City = Crash_City
        self.Crash_Date = Crash_Date
        self.Dispatch_Time = Dispatch_Time
        self.Gps_Other = Gps_Other
        self.Latitude = Latitude
        self.Longitude = Longitude
        self.Loss_Cross_Street = Loss_Cross_Street
        self.Loss_State_Abbr = Loss_State_Abbr
        self.Loss_Street = Loss_Street
        self.Photographs_Taken = Photographs_Taken
        self.Report_Type_Id = Report_Type_Id              
        self.Weather_Condition = Weather_Condition
        self.Incident_Hit_and_Run = Incident_Hit_and_Run
        for key in kwargs:
            setattr(self, key, kwargs[key])        


class People(object):
    def __init__(self, Address: str, Address2: str, City: str, Condition_At_Time_Of_Crash: str, Contributing_Circumstances_Person:str, 
                 Date_Of_Birth:str, Driver_Distracted_By: str, Drivers_License_Jurisdiction: str, Drivers_License_Number:str, Ejection: str,
                 First_Name: str, Home_Phone: str, Injury_Status: str, Last_Name: str, Middle_Name: str, Name_Suffix:str, Person_Type:str, 
                 Safety_Equipment_Restraint: str, State:str, Unit_Number:str, Zip_Code: str, **kwargs):
        
        self.Address = Address
        self.Address2 = Address2
        self.City = City
        self.Condition_At_Time_Of_Crash = Condition_At_Time_Of_Crash
        self.Contributing_Circumstances_Person = Contributing_Circumstances_Person
        self.Date_Of_Birth = Date_Of_Birth
        self.Driver_Distracted_By = Driver_Distracted_By
        self.Drivers_License_Jurisdiction = Drivers_License_Jurisdiction
        self.Drivers_License_Number = Drivers_License_Number
        self.Ejection = Ejection
        self.First_Name = First_Name
        self.Home_Phone = Home_Phone
        self.Injury_Status = Injury_Status
        self.Last_Name = Last_Name
        self.Middle_Name = Middle_Name
        self.Name_Suffix = Name_Suffix
        self.Person_Type = Person_Type
        self.Safety_Equipment_Restraint = Safety_Equipment_Restraint
        self.State = State
        self.Unit_Number = Unit_Number
        self.Zip_Code = Zip_Code
        for key in kwargs:
            setattr(self, key, kwargs[key])        
        
class Vehicle(object):
    def __init__(self, Air_Bag_Deployed:str, Contributing_Circumstances_Vehicle:str, Damaged_Areas:str, Insurance_Company:str, Insurance_Expiration_Date:str,
                  Insurance_Policy_Number:str, License_Plate: str, Make:str, Make_Original:str, Model:str, Model_Original:str, Model_Year:str, 
                  Model_Year_Original:str, Posted_Statutory_SpeedLimit: str, Registration_State:str, Road_Surface_Condition:str,
                  VIN:str, VIN_Original:str, Vehicle_Towed:str, VinValidation_VinStatus:str, **kwargs):

        self.Air_Bag_Deployed = Air_Bag_Deployed
        self.Contributing_Circumstances_Vehicle = Contributing_Circumstances_Vehicle
        self.Damaged_Areas = Damaged_Areas
        self.Insurance_Company = Insurance_Company
        self.Insurance_Expiration_Date = Insurance_Expiration_Date
        self.Insurance_Policy_Number = Insurance_Policy_Number
        self.License_Plate = License_Plate
        self.Make = Make    
        self.Make_Original = Make_Original
        self.Model = Model
        self.Model_Original = Model_Original
        self.Model_Year = Model_Year
        self.Model_Year_Original = Model_Year_Original
        self.Posted_Statutory_SpeedLimit = Posted_Statutory_SpeedLimit
        self.Registration_State = Registration_State
        self.Road_Surface_Condition = Road_Surface_Condition
        self.VIN = VIN
        self.VIN_Original = VIN_Original
        self.Vehicle_Towed = Vehicle_Towed
        self.VinValidation_VinStatus = VinValidation_VinStatus  
        for key in kwargs:
            setattr(self, key, kwargs[key])   
            
            
class Citations(object):
    def __init__(self,Violation_Code:str, Party_Id:str,Unit_Number:str):
        self.Violation_Code = Violation_Code
        self.Party_Id = Party_Id
        self.Unit_Number = Unit_Number

class Report(object):
    def __init__(self, FormName: str, CountKeyed: int, Incident: Incident, People: List, Vehicles: List, Citations: List):
        self.FormName = FormName
        self.CountKeyed = CountKeyed
        self.Incident = Incident
        self.People = People
        self.Vehicles = Vehicles
        self.Citations = Citations

class MainCls(object):
    def __init__(self, Report: Report):
        self.Report = Report    
        
         
def _get_crash_city(df):
    try:
        city = get_value_from_df(df,"City").strip()
        if(len(city) != 0):
            inside = get_value_from_df(df,"Inside_Check")
            if(inside == "Checked"):
                is_city = True
            else:
                is_city = False
                
        if(is_city):
            city = city
        else:
            city = ""
    except:
        city = ""
    return city            
       

def isTowed(driver_list, driver_no):
    text = get_driver_details_in_each_segment(driver_list,'Removed_By', driver_no)   
    if(len(text) > 2):
        return "1"
    else:
        return "0"
    
def get_VIN_status(text):
    if(len(text)> 4):
        st = "V"
    else:
        st = ""
    return st


def get_incident_hit_run(df):
    is_checked = get_value_from_df(df,"Hit_Run_Check")
    if(is_checked == "Checked"):
        check = "1"
    else:
        check = "0"
    return check

def capture_loss_cross_street(df):
    try:
        loss_cross_street = ""
        at_intersection = get_value_from_df(df,"Intersect_At_Check")
        if(at_intersection=="Checked"):
            loss_cross_street = get_value_from_df(df,"On_Intersecting_Road_Street")
        else:
            loss_cross_street = ""
    except:
        loss_cross_street = ""
    return loss_cross_street  

def capture_photos_taken(df):
    try:
        taken = ""
        taken_yes_value = get_value_from_df(df,"Photos_Taken_Y_Check")
        taken_no_value = get_value_from_df(df,"Photos_Taken_N_Check")
        
        if(taken_yes_value=="Checked"):
            taken = "Y"
        elif(taken_no_value=="Checked"):
            taken = "N"
        else:
            taken = ""
    except:
        taken = ""
    return taken  


def get_driver_params(df, driver_infos_list):
    label_count_dict = dict()
    list_of_labels = driver_infos_list
    for label in list_of_labels:
        label_count_dict[label] = get_label_count(df, label)
    try:
        all_driver_count = mode(list(label_count_dict.values()))
    except:
        all_driver_count = max(list(label_count_dict.values()))
    
    for i in range(all_driver_count):
        globals()["driver_{}".format(i+1)] = dict()
        
    total_pages = df['page_no'].nunique()
    page_count = 1
    for page_no in range(total_pages):
        page_label_count_dict = dict()
        group_df = df.groupby('page_no')
        try:
            page_df = group_df.get_group('page_no_{}'.format(page_no+1))
        except:
            continue
        for label in list_of_labels:
            page_label_count_dict[label] = get_label_count(page_df, label)
        try:
            page_driver_count = mode(list(page_label_count_dict.values()))
        except:
            page_driver_count = max(list(page_label_count_dict.values()))
        if(page_driver_count != 0):
            if(page_label_count_dict['Person_Driver_Check'] == page_driver_count):
                check_label = 'Person_Driver_Check'
                
            elif(page_label_count_dict['Person_Driverless_Check'] == page_driver_count):
                check_label = 'Person_Driverless_Check'
    
            elif(page_label_count_dict['Person_Ejected_Check'] == page_driver_count):
                check_label = 'Person_Ejected_Check'
    
            elif(page_label_count_dict['Person_Extricated_Check'] == page_driver_count):
                check_label = 'Person_Extricated_Check'
    
            elif(page_label_count_dict['Driver_License_No_DL_Check'] == page_driver_count):
                check_label = 'Driver_License_No_DL_Check'
    
            elif(page_label_count_dict['Driver_License_No_Not_Valid_Check'] == page_driver_count):
                check_label = 'Driver_License_No_Not_Valid_Check'            
                
    
            else:
                idx = list(page_label_count_dict.values()).index(page_driver_count)
                check_label = list(page_label_count_dict.keys())[idx]
    
            check_label_ymin = []
    
            for i,row in page_df.iterrows():
                if(row.label == check_label):
                    check_label_ymin.append(row.ymin)
                else:
                    continue
    
            check_label_ymin = sorted(check_label_ymin)
    
            for i in range(len(check_label_ymin)):
                globals()["ymin_{}".format(i+1)] = check_label_ymin[i]
    
            factor = (page_driver_count*page_count) - page_driver_count
    
    
            for i, row in page_df.iterrows():
                
                if(row.label in driver_infos_list):
                    label = row.label
    
                    y_min = row.ymin
    
                    for a in range(len(check_label_ymin)):
                        if(a !=  len(check_label_ymin)-1 and y_min >=  (globals()["ymin_{}".format(a+1)]-2) and y_min <=  (globals()["ymin_{}".format(a+2)]-2)):
                            globals()['driver_{}'.format(a+1+factor)][label] = row.text
                            # globals()["driver_{}".format(count_driver)] = globals()['page_driver_{}'.format(a+1)]
    
                        elif(a == len(check_label_ymin)-1):
                            globals()['driver_{}'.format(a+1+factor)][label] = row.text
                            # globals()["driver_{}".format(count_driver)] = globals()['page_driver_{}'.format(len(check_label_ymin))]
             
        #     d1 = 1 + factor
        #     d2 = 2 + factor
        #     for i, row in page_df.iterrows():
                
        #         if(row.label in code_list):
        #             label = row.label
        #             text = row.text
                    
        #             if(int(row.label.split("_")[-1]) == 1):
        #                 globals()['driver_{}'.format(d1)][label] = text
        #             elif(int(row.label.split("_")[-1]) == 2):
        #                 globals()['driver_{}'.format(d2)][label] = text
        #     page_count +=  1
        # else:
        #     continue

    driver_list = [globals()['driver_{}'.format(i+1)] for i in range(all_driver_count)]
    return driver_list, all_driver_count 


def _get_code_desc_from_mapping_df_non_unit(df, mapping_df, label_name, key_name):
    try:
        globals()["{}".format(key_name)]  =  {x.split("=")[0].upper().strip(' '): x.split("=")[1].strip(' ') for x in list(mapping_df[key_name].values) if (type(x) == str)}
        search_values = [label_name]
        filter_df = df[df.label.str.contains('|'.join(search_values))]
        code_list = []
        desc_list = []
        dic_list = []
        
        for i, row in filter_df.iterrows():
            if(row.text == "Checked"):
                code = row.label.split("_")[-1]
                desc = get_dic_value(globals()["{}".format(key_name)],code)
                code_list.append(code)
                desc_list.append(desc)
                    
        
        if(len(code_list) > 0):
            for i in range(len(code_list)):
                dic = dict()
                dic['Code'] = code_list[i]
                dic['Description'] = desc_list[i]
                dic_list.append(dic)
    except:
        dic_list = []
    return dic_list
        
def _get_code_desc_from_mapping_df_unit(df, mapping_df, label_names, key_name, unit_no):
    try:
        globals()["{}".format(key_name)]  =  {x.split("=")[0].upper().strip(' '): x.split("=")[1].strip(' ') for x in list(mapping_df[key_name].values) if (type(x) == str)}
        label_names = label_names.format(unit_no)
        search_values = [label_names]
        filter_df = df[df.label.str.contains('|'.join(search_values))]
        code_list = []
        desc_list = []
        dic_list = []
        
        for i, row in filter_df.iterrows():
            if(row.text == "Checked"):
                code = row.label.split("_")[-1]
                desc = get_dic_value(globals()["{}".format(key_name)],code)
                code_list.append(code)
                desc_list.append(desc)
                    
        
        if(len(code_list) > 0):
            for i in range(len(code_list)):
                dic = dict()
                dic['Code'] = code_list[i]
                dic['Description'] = desc_list[i]
                dic_list.append(dic)
    except:
        dic_list = []
    return dic_list


def _get_code_desc_from_mapping_dic(dic, mapping_df, label_name, key_name):
    globals()["{}".format(key_name)]  =  {x.split("=")[0].upper().strip(' '): x.split("=")[1].strip(' ') for x in list(mapping_df[key_name].values) if (type(x) == str)}
    value_from_dic = get_value_from_dict(dic, label_name)
    code = re.sub('[^0-9]','',value_from_dic)
    desc = get_dic_value(globals()["{}".format(key_name)],code)
    
    if((len(code.strip()) == 0 and len(desc.strip()) == 0) or len(desc.strip()) == 0):
        return_list = []
    else:
        dic_ = dict()
        dic_['Code'] = code
        dic_['Description'] = desc
        return_list = [dic_]
        
    return return_list

def get_ejection_status(text):
    if(text == "Checked"):
        eject = "Ejected, Totally"
    else:
        eject = "Not Applicable"
    return eject




def get_air_bag_status(text):
    text = text.strip()
    if(text == "0" or text == "O" or text == "o"):
        air_bag = "NA"
    elif(text == "1" or text == "2" or text == "3" or text == "4" or text == "5" or text == "6"):
        air_bag = "YES"
    elif(text == "7"):
        air_bag = "NO"
    else:
        air_bag = ""
    return air_bag

            
def get_injury_status(text):
    try:
        text = text.strip()
        if(text == "1"):
            inj = "No Injury"
        elif(text == '2'):
            inj = "Possible Injury"
        elif(text == "3"):
            inj = "Non-Incapacitating Injury"
        elif(text == "4"):
            inj = "Incapacitating Injury"
        elif(text == '5'):
            inj = "Fatal Injury"
        elif(text == "U" or text == "u"):
            inj = "Unknown/Not Reported"
        else:
            inj = "Unknown/Not Reported"
    except:
        inj = "Unknown/Not Reported"
    return inj
    

def get_witness_params(df, witness_infos_list):        
    label_count_dict = dict()

    list_of_labels = witness_infos_list
    
    for label in list_of_labels:
        label_count_dict[label] = get_label_count(df, label)

    try:
        all_witness_count = mode(list(label_count_dict.values()))
    except:
        all_witness_count = max(list(label_count_dict.values()))


    for i in range(all_witness_count):
        globals()["witness_{}".format(i+1)] = dict()
        
    total_pages = df['page_no'].nunique()
    page_count = 1
    for page_no in range(total_pages):
        page_label_count_dict = dict()
        group_df = df.groupby('page_no')
        try:
            page_df = group_df.get_group('page_no_{}'.format(page_no+1))
        except:
            continue

        for label in list_of_labels:
            page_label_count_dict[label] = get_label_count(page_df, label)

        try:
            page_witness_count = mode(list(page_label_count_dict.values()))
        except:
            page_witness_count = max(list(page_label_count_dict.values()))
            

        if(page_witness_count != 0):
            if(page_label_count_dict['Witness_Details'] == page_witness_count):
                check_label = 'Witness_Details'
                
            elif(page_label_count_dict['Witness_DOB'] == page_witness_count):
                check_label = 'Witness_DOB'
        
            else:
                idx = list(page_label_count_dict.values()).index(page_witness_count)
                check_label = list(page_label_count_dict.keys())[idx]
    
            check_label_ymin = []
    
            for i,row in page_df.iterrows():
                if(row.label == check_label):
                    check_label_ymin.append(row.ymin)
                else:
                    continue
    
            check_label_ymin = sorted(check_label_ymin)
    
            for i in range(len(check_label_ymin)):
                globals()["ymins_{}".format(i+1)] = check_label_ymin[i]
    
            factor = (page_witness_count*page_count) - page_witness_count
    
    
            for i, row in page_df.iterrows():
                
                if(row.label in witness_infos_list):
                    label = row.label
    
                    y_min = row.ymin
    
                    for a in range(len(check_label_ymin)):
                        if(a !=  len(check_label_ymin)-1 and y_min >=  (globals()["ymins_{}".format(a+1)]-1) and y_min <=  (globals()["ymins_{}".format(a+2)]-2)):
                            globals()['witness_{}'.format(a+1)][label] = row.text
    
                        elif(a == len(check_label_ymin)-1):
                            globals()['witness_{}'.format(a+1)][label] = row.text             
        else:
            continue

    witness_list = [globals()['witness_{}'.format(i+1)] for i in range(all_witness_count)]
    return witness_list, all_witness_count        
        
        
def incident_extraction(df, incident_list, mapping_df):
    try:
        incident = Incident(Case_Identifier = get_value_from_df(df,"Agency_Report_Number"),
                            Crash_City = _get_crash_city(df),
                            Crash_Date = get_value_from_df(df,"Report_Date"),
                            Dispatch_Time = get_value_from_df(df,"Time_Invest"), #
                            Gps_Other = "",
                            Latitude = get_value_from_df(df,"Latitude"),
                            Longitude = get_value_from_df(df,"Longitude"),
                            Loss_Cross_Street = capture_loss_cross_street(df),
                            Loss_State_Abbr = "AZ",
                            Loss_Street = get_value_from_df(df,"On_Highway_Road_Street"),
                            Photographs_Taken = capture_photos_taken(df), 
                            Report_Type_Id = "A",
                            Weather_Condition = _get_code_desc_from_mapping_df_non_unit(df, mapping_df, "Weather_Condition_", "Weather_Condition"),
                            Incident_Hit_and_Run = get_incident_hit_run(df))
        
        if(incident.Incident_Hit_and_Run != '1'):
            del incident.Incident_Hit_and_Run    
            
        incident_list.append(incident)
        return(incident_list)
    except:
        print("Error : Error in incident_extraction()")
        return sys.exc_info(), "error"
    
def _get_code_desc_from_mapping_df_damage(df, mapping_df, label_names, key_name, unit_no):
    try:
        globals()["{}".format(key_name)]  =  {x.split("=")[0].upper().strip(' '): x.split("=")[1].strip(' ') for x in list(mapping_df[key_name].values) if (type(x) == str)}
        label_names = label_names.format(unit_no)
        search_values = [label_names]
        filter_df = df[df.label.str.contains('|'.join(search_values))]
        code_list = []
        desc_list = []
        dic_list = []
        
        for i, row in filter_df.iterrows():
            if(row.text == "Checked"):
                code = row.label.split("_")[-1]
                desc = get_dic_value(globals()["{}".format(key_name)],code)
                code_list.append(code)
                desc_list.append(desc)
                    
        
        if(len(desc_list) == 0):
            damage_area = "NA"
        if(len(desc_list) == 1):
            damage_area = desc_list[0]
        elif(len(desc_list) > 1):
            if("Front" in desc_list and "Rear" in desc_list):
                damage_area = "Front & Rear"
            else:
                damage_area = "MULTIPLE"
        else:
            damage_area = "NA"
    except:
        damage_area = "NA"
    return damage_area
    
            
def driver_extraction(df, driver_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df):
    try:
        global party_number
        
        for i in range(len(driver_list)):
            dic = driver_list[i]
            person_type = get_person_type(dic)
            
            
            
            if(((i+1) % 2) == 1):
                code_n = 1
            elif(((i+1) % 2) == 0):
                code_n = 2
            
            #Name
            Same_as_Driver_name_check = get_driver_details_in_each_segment(driver_list,'Owner_Same_As_Driver_Check',(i+1))
            if(Same_as_Driver_name_check == "Checked"):
                Same_as_Driver_GUI = 'Y'
            else:
                Same_as_Driver_GUI = ''
                
            #Address
            v_owner_name_add = get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1))
            driver_name = get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1))
            driver_add = get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1))
            driver_name_add = driver_name + " " + driver_add
            
            if(fuzz.token_sort_ratio(v_owner_name_add,driver_name_add) > 67):
                Address_Same_as_Owner_GUI = 'Y'
            else:
                Address_Same_as_Owner_GUI = ''
 
            _get_code_desc_from_mapping_df_unit(df, mapping_df, "Cond_Diver_U{}_", "Condition_At_Time_Of_Crash", str(i+1))
                
            if(person_type == "DRIVER"):
                people_info = People(Address  = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['address'],
                                    Address2 = "",
                                    City = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['city'],
                                    Condition_At_Time_Of_Crash = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Cond_Diver_U{}_", "Condition_At_Time_Of_Crash", str(i+1)),
                                    Contributing_Circumstances_Person = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Violations_Unit_U", "Contributing_Circumstances_Person", str(i+1)),
                                    Date_Of_Birth = get_driver_details_in_each_segment(driver_list,'Person_DOB',(i+1)),
                                    Driver_Distracted_By = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Distract_Driver_Behaviour_U{}_", "Driver_Distracted_By", str(i+1)),
                                    Drivers_License_Jurisdiction = get_driver_details_in_each_segment(driver_list,'Driver_State',(i+1)),
                                    Drivers_License_Number = get_driver_details_in_each_segment(driver_list,'Driver_License_No',(i+1)),
                                    Ejection = get_ejection_status(get_driver_details_in_each_segment(driver_list,'Person_Ejected_Check',(i+1))),
                                    First_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['first_name'],
                                    Home_Phone = get_driver_details_in_each_segment(driver_list,'Person_Telephone_Number',(i+1)),
                                    Injury_Status = get_injury_status(get_driver_details_in_each_segment(driver_list,'Injury_Severity',(i+1))),
                                    Last_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['last_name'],
                                    Middle_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                    Name_Suffix = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['suffix'],
                                    Person_Type = get_person_type(dic),
                                    Safety_Equipment_Restraint = _get_code_desc_from_mapping_dic(dic, mapping_df, "Safety_Devices", "Safety_Equipment_Restraint"),
                                    State = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['state'],
                                    Unit_Number = str(i+1),
                                    Zip_Code = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['zipcode'],
                                    Same_as_Driver_GUI = Same_as_Driver_GUI,
                                    Address_Same_as_Owner_GUI = Address_Same_as_Owner_GUI)
                
            else:
                people_info = People(Address  = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['address'],
                                    Address2 = "",
                                    City = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['city'],
                                    Condition_At_Time_Of_Crash = [],
                                    Contributing_Circumstances_Person = [],
                                    Date_Of_Birth = get_driver_details_in_each_segment(driver_list,'Person_DOB',(i+1)),
                                    Driver_Distracted_By = [],
                                    Drivers_License_Jurisdiction = get_driver_details_in_each_segment(driver_list,'Driver_State',(i+1)),
                                    Drivers_License_Number = get_driver_details_in_each_segment(driver_list,'Driver_License_No',(i+1)),
                                    Ejection = get_ejection_status(get_driver_details_in_each_segment(driver_list,'Person_Ejected_Check',(i+1))),
                                    First_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['first_name'],
                                    Home_Phone = get_driver_details_in_each_segment(driver_list,'Person_Telephone_Number',(i+1)),
                                    Injury_Status = get_driver_details_in_each_segment(driver_list,'Injury_Severity',(i+1)),
                                    Last_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['last_name'],
                                    Middle_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                    Name_Suffix = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['suffix'],
                                    Person_Type = get_person_type(dic),
                                    Safety_Equipment_Restraint =  [],
                                    State = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['state'],
                                    Unit_Number = str(i+1),
                                    Zip_Code = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['zipcode'],
                                    Same_as_Driver_GUI = Same_as_Driver_GUI,
                                    Address_Same_as_Owner_GUI = Address_Same_as_Owner_GUI)
                
            if(len(people_info.Address.strip()) >  0 or len(people_info.First_Name.strip()) > 0 or \
                len(people_info.Middle_Name.strip()) > 0 or len(people_info.Last_Name['Code'].strip()) > 0 or len(people_info.Date_Of_Birth.strip()) > 0 or \
                len(people_info.Drivers_License_Jurisdiction.strip()) > 0):
                
                party_number += 1
                people_info.Party_Id = str(party_number)
                
                if(people_info.Same_as_Driver_GUI != 'Y'):
                    del people_info.Same_as_Driver_GUI
                    
                if(people_info.Address_Same_as_Owner_GUI != 'Y'):
                    del people_info.Address_Same_as_Owner_GUI
                    
                people_list.append(people_info)
    except:
        return people_list
    return people_list


def vehicle_detail_extraction(df,driver_list,vehicle_list, city_lookup_path, mapping_df):
    try:
        for i in range(len(driver_list)):
            dic = driver_list[i]
            if(((i+1) % 2) == 1):                 
                code_n = 1
            elif(((i+1) % 2) == 0):
                code_n = 2
            vehicle_info = Vehicle(Air_Bag_Deployed = get_air_bag_status(get_driver_details_in_each_segment(driver_list,'Air_Bag',(i+1))),
                                  Contributing_Circumstances_Vehicle =  _get_code_desc_from_mapping_df_unit(df, mapping_df, "Motor_Vehicle_U{}_", "Contributing_Circumstances_Vehicle", str(i+1)),
                                  Damaged_Areas = _get_code_desc_from_mapping_df_damage(df, mapping_df, "Unit_No_{}_Damage_Area_", "Damage_Areas", str(i+1)),
                                  Insurance_Company = get_driver_details_in_each_segment(driver_list,'Insurance_Company',(i+1)),
                                  Insurance_Expiration_Date = "",
                                  Insurance_Policy_Number = get_driver_details_in_each_segment(driver_list,'Insurance_Policy_Number',(i+1)),
                                  License_Plate =  get_driver_details_in_each_segment(driver_list,'Vehicle_Plate_Number',(i+1)),
                                  Make = get_driver_details_in_each_segment(driver_list,'Vehicle_Make',(i+1)),
                                  Make_Original = get_driver_details_in_each_segment(driver_list,'Vehicle_Make',(i+1)),
                                  Model = get_driver_details_in_each_segment(driver_list,'Vehicle_Model',(i+1)),
                                  Model_Original = get_driver_details_in_each_segment(driver_list,'Vehicle_Model',(i+1)),
                                  Model_Year = get_driver_details_in_each_segment(driver_list,'Vehicle_Year',(i+1)),
                                  Model_Year_Original = get_driver_details_in_each_segment(driver_list,'Vehicle_Year',(i+1)),
                                  Posted_Statutory_SpeedLimit = get_driver_details_in_each_segment(driver_list,'Posted_Speed_Limit',(i+1)) ,
                                  Registration_State = get_driver_details_in_each_segment(driver_list,'Driver_State',(i+1)), #Wrie code for Registration State
                                  Road_Surface_Condition = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Road_Surface_Cond_U{}_", "Road_Surface_Condition", str(i+1)),
                                  Unit_Number = str(i+1),
                                  VIN = get_driver_details_in_each_segment(driver_list,'Vehicle_VIN',(i+1)),
                                  VIN_Original  = get_driver_details_in_each_segment(driver_list,'Vehicle_VIN',(i+1)),
                                  Vehicle_Towed = isTowed(driver_list, (i+1)),
                                  VinValidation_VinStatus = get_VIN_status(get_driver_details_in_each_segment(driver_list,'Vehicle_VIN',(i+1))))
            
            if(len(vehicle_info.Insurance_Company.strip()) > 0 or \
                len(vehicle_info.Make.strip()) > 0 or len(vehicle_info.Model_Year['Code'].strip()) > 0 or len(vehicle_info.VIN.strip()) > 0 or \
                len(vehicle_info.License_Plate.strip()) > 0):
                vehicle_list.append(vehicle_info)
    except:
        return(vehicle_list)
    return(vehicle_list)



def trailer_detail_extraction(df,driver_list,vehicle_list, city_lookup_path, mapping_df, unit_no):
    try:
        for i in range(len(driver_list)):
            dic = driver_list[i]
            vehicle_info = Vehicle(Air_Bag_Deployed = "",
                                  Contributing_Circumstances_Vehicle =  [],
                                  Damaged_Areas = "",
                                  Insurance_Company = "",
                                  Insurance_Expiration_Date = "",
                                  Insurance_Policy_Number = "",
                                  License_Plate =  get_driver_details_in_each_segment(driver_list,'Trailer_Plate_No',(i+1)),
                                  Make = "",
                                  Make_Original = "",
                                  Model = "",
                                  Model_Original = "",
                                  Model_Year = get_driver_details_in_each_segment(driver_list,'Trailer_Year',(i+1)),
                                  Model_Year_Original = get_driver_details_in_each_segment(driver_list,'Trailer_Year',(i+1)),
                                  Posted_Statutory_SpeedLimit = "" ,
                                  Registration_State = get_driver_details_in_each_segment(driver_list,'Trailer_State',(i+1)), #Wrie code for Registration State
                                  Road_Surface_Condition = [],
                                  VIN = "",
                                  VIN_Original  = "",
                                  Vehicle_Towed = "",
                                  VinValidation_VinStatus = "")
            
            if(len(vehicle_info.Model_Year.strip()) >  0 or len(vehicle_info.Registration_State.strip()) > 0 or len(vehicle_info.License_Plate.strip()) > 0):
                unit_no += 1
                vehicle_info.Unit_Number = str(unit_no)
                vehicle_list.append(vehicle_info)
    except:
        return(vehicle_list, unit_no)
    return(vehicle_list, unit_no)



#driver_list for wner address check
#def passenger_extraction(df, driver_list, passenger_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df, total_party)
def passenger_extraction(df, driver_list, passenger_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df, party_number):
    try:
        party_number = int(party_number)
        for i in range(len(passenger_list)):
            dic = passenger_list[i]

            people_info = People(Address = name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['address'],
                                Address2 = "",
                                City = name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['city'],
                                Condition_At_Time_Of_Crash = [],
                                Contributing_Circumstances_Person = [],
                                Date_Of_Birth = get_passenger_details_in_each_segment(passenger_list,'Pass_DOB',(i+1)),
                                Driver_Distracted_By = [],
                                Drivers_License_Jurisdiction = "",
                                Drivers_License_Number = "",
                                Ejection = "",
                                First_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['first_name'],
                                Home_Phone = get_passenger_details_in_each_segment(passenger_list,'Pass_Telephone',(i+1)),
                                Injury_Status = "",
                                Last_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['last_name'],
                                Middle_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                Name_Suffix = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['suffix'],
                                Person_Type = "PASSENGER",
                                Safety_Equipment_Restraint =  _get_code_desc_from_mapping_dic(dic, mapping_df, "Pass_Seat_SD", "Safety_Equipment_Restraint"),
                                State = name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['state'],
                                Unit_Number = get_passenger_details_in_each_segment(passenger_list,'Passenger_Unit',(i+1)),
                                Zip_Code = name_address_split_using_lookup(get_passenger_details_in_each_segment(passenger_list,'Pass_Name_Details',(i+1)), city_lookup_path)['zipcode'])

            if(len(people_info.Address.strip()) >  0 or len(people_info.First_Name.strip()) > 0 or len(people_info.Safety_Equipment_Restraint) > 0 or \
               len(people_info.City.strip()) > 0 or len(people_info.Date_Of_Birth.strip()) > 0 or len(people_info.Home_Phone.strip()) > 0 or \
                   len(people_info.Zip_Code.strip()) > 0):
                party_number += 1
                people_info.Party_Id = str(party_number)
                people_list.append(people_info)
    except:
        return people_list
    return people_list, party_number


def process_phone_number(text):
    try:
        text = re.sub('[^0-9]', '', text)
        text = text[:10]
        out = text[:3] + "-" + text[3:6] + "-" + text[6:]
        text = out
    except:
        text = ""
    return text

def witness_extraction(df, driver_list, witness_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df, party_number):
    try:
        party_number = party_number
        for i in range(len(witness_list)):
            dic = witness_list[i]

            people_info = People(Address = name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['address'],
                                Address2 = "",
                                City = name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['city'],
                                Condition_At_Time_Of_Crash = [],
                                Contributing_Circumstances_Person = [],
                                Date_Of_Birth = get_passenger_details_in_each_segment(witness_list,'Witness_DOB',(i+1)).strip(),
                                Driver_Distracted_By = [],
                                Drivers_License_Jurisdiction = "",
                                Drivers_License_Number = "",
                                Ejection = "",
                                First_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['first_name'],
                                Home_Phone = process_phone_number(name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['phone']),
                                Injury_Status = "",
                                Last_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['last_name'],
                                Middle_Name = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                Name_Suffix = name_split_using_pattern(name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['suffix'],
                                Person_Type = "WITNESS",
                                Safety_Equipment_Restraint =  [],
                                State = name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['state'],
                                Unit_Number = "",
                                Zip_Code = name_address_split_using_lookup(get_passenger_details_in_each_segment(witness_list,'Witness_Details',(i+1)), city_lookup_path)['zipcode'])

            if(len(people_info.Address.strip()) >  0 or len(people_info.City.strip()) > 0 or len(people_info.First_Name.strip()) > 0 or \
               len(people_info.Last_Name.strip()) > 0 or len(people_info.State.strip()) > 0 or len(people_info.Zip_Code.strip()) > 0):
                party_number += 1
                people_info.Party_Id = str(party_number)
                del people_info.Unit_Number
                people_list.append(people_info)
    except:
        return people_list
    return people_list


def vehicle_owner_extraction(df, driver_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df):
    try:
        global party_number
        for i in range(len(driver_list)):
            dic = driver_list[i]
            person_type = get_person_type(dic)
            
            #Name
            v_owner_check = get_driver_details_in_each_segment(driver_list,'Owner_Same_As_Driver_Check',(i+1))
            

            if(v_owner_check == "Checked"):
                Same_as_Driver_GUI = 'Y'
            else:
                Same_as_Driver_GUI = ''
                
            #Address
            v_owner_name_add = get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1))
            v_owner_address = name_address_split_using_lookup(v_owner_name_add, city_lookup_path)['address']
            driver_add = get_driver_details_in_each_segment(driver_list,'Person_Address',(i+1))
            

            
            if(fuzz.token_sort_ratio(v_owner_address,driver_add) > 80 or "SAME" in v_owner_name_add.upper()):
                Address_Same_as_Driver_GUI = 'Y'
            else:
                Address_Same_as_Driver_GUI = ''
                
            if(Same_as_Driver_GUI == 'Y'):
                people_info = People(Address  = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['address'],
                                    Address2 = "",
                                    City = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['city'],
                                    Condition_At_Time_Of_Crash = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Cond_Diver_U{}_", "Condition_At_Time_Of_Crash", str(i+1)),
                                    Contributing_Circumstances_Person = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Violations_Unit_U", "Contributing_Circumstances_Person", str(i+1)),
                                    Date_Of_Birth = get_driver_details_in_each_segment(driver_list,'Person_DOB',(i+1)),
                                    Driver_Distracted_By = _get_code_desc_from_mapping_df_unit(df, mapping_df, "Distract_Driver_Behaviour_U{}_", "Driver_Distracted_By", str(i+1)),
                                    Drivers_License_Jurisdiction = get_driver_details_in_each_segment(driver_list,'Driver_State',(i+1)),
                                    Drivers_License_Number = get_driver_details_in_each_segment(driver_list,'Driver_License_No',(i+1)),
                                    Ejection = get_ejection_status(get_driver_details_in_each_segment(driver_list,'Person_Ejected_Check',(i+1))),
                                    First_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['first_name'],
                                    Home_Phone = get_driver_details_in_each_segment(driver_list,'Person_Telephone_Number',(i+1)),
                                    Injury_Status = get_injury_status(get_driver_details_in_each_segment(driver_list,'Injury_Severity',(i+1))),
                                    Last_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['last_name'],
                                    Middle_Name = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                    Name_Suffix = name_split_using_pattern(get_driver_details_in_each_segment(driver_list,'Person_Name',(i+1)),business_name_lookup_path=business_name_lookup_path)['suffix'],
                                    Person_Type = "VEHICLE OWNER",
                                    Safety_Equipment_Restraint = _get_code_desc_from_mapping_dic(dic, mapping_df, "Safety_Devices", "Safety_Equipment_Restraint"),
                                    State = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['state'],
                                    Unit_Number = str(i+1),
                                    Zip_Code = name_address_split_using_lookup("PERSON NAME "+get_driver_details_in_each_segment(driver_list,'Person_Address_Details',(i+1)), city_lookup_path)['zipcode'],
                                    Same_as_Driver_GUI = Same_as_Driver_GUI,
                                    Address_Same_as_Driver_GUI = Address_Same_as_Driver_GUI)
            
            else:
                name_add_text = get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1))
                name_text = name_address_split_using_lookup(name_add_text, city_lookup_path)['names']
                
                people_info = People(Address  = name_address_split_using_lookup("OWNER NAME "+get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1)), city_lookup_path)['address'],
                                    Address2 = "",
                                    City = name_address_split_using_lookup("OWNER NAME "+get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1)), city_lookup_path)['city'],
                                    Condition_At_Time_Of_Crash = [],
                                    Contributing_Circumstances_Person = [],
                                    Date_Of_Birth = "",
                                    Driver_Distracted_By = [],
                                    Drivers_License_Jurisdiction = "",
                                    Drivers_License_Number = "",
                                    Ejection = "",
                                    First_Name = name_split_using_pattern(name_text,business_name_lookup_path=business_name_lookup_path)['first_name'],
                                    Home_Phone = "",
                                    Injury_Status = "",
                                    Last_Name = name_split_using_pattern(name_text,business_name_lookup_path=business_name_lookup_path)['last_name'],
                                    Middle_Name = name_split_using_pattern(name_text,business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                    Name_Suffix = name_split_using_pattern(name_text,business_name_lookup_path=business_name_lookup_path)['suffix'],
                                    Person_Type = "VEHICLE OWNER",
                                    Safety_Equipment_Restraint =[],
                                    State = name_address_split_using_lookup("OWNER NAME "+get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1)), city_lookup_path)['state'],
                                    Unit_Number = str(i+1),
                                    Zip_Code = name_address_split_using_lookup("OWNER NAME "+get_driver_details_in_each_segment(driver_list,'Owner_Details',(i+1)), city_lookup_path)['zipcode'],
                                    Same_as_Driver_GUI = Same_as_Driver_GUI,
                                    Address_Same_as_Driver_GUI = Address_Same_as_Driver_GUI)
                
            if(len(people_info.Address.strip()) >  0 or len(people_info.First_Name.strip()) > 0 or \
                len(people_info.Middle_Name.strip()) > 0 or len(people_info.Last_Name['Code'].strip()) > 0 or len(people_info.Date_Of_Birth.strip()) > 0 or \
                len(people_info.Drivers_License_Jurisdiction.strip()) > 0 or len(people_info.State.strip()) > 0):  
                
                party_number += 1
                people_info.Party_Id = str(party_number)
                
                if(people_info.Same_as_Driver_GUI != 'Y'):
                    del people_info.Same_as_Driver_GUI
                if(people_info.Address_Same_as_Driver_GUI != 'Y'):
                    del people_info.Address_Same_as_Driver_GUI
                    
                people_list.append(people_info)
    except:
        return people_list
    return people_list
 

def property_extraction(df, people_list, vehicle_list, passenger_list, city_lookup_path, business_name_lookup_path, party_number, unit_no):

    total_pages_in_df = df["page_no"].nunique()
    unit_no = unit_no
    party_number = party_number
    
    for page_no in range(1,total_pages_in_df+1):
        page_df = df[df["page_no"] == "page_no_{}".format(page_no)]
        if("Property_Owner_Details" in page_df['label'].values or "Property_Owner_Telephone" in page_df['label'].values):
            unit_no += 1
            p_damage_info = People(Address = "",
                            Address2 = "",
                            City = "",
                            Condition_At_Time_Of_Crash = [],
                            Contributing_Circumstances_Person = [],
                            Date_Of_Birth = "",
                            Driver_Distracted_By = [],
                            Drivers_License_Jurisdiction = "",
                            Drivers_License_Number = "",
                            Ejection = "",
                            First_Name = "",
                            Home_Phone = "",
                            Injury_Status = "",
                            Last_Name = get_value_from_df(page_df,"Prop_Damage_Value"),
                            Middle_Name = "",
                            Name_Suffix = "",
                            Person_Type = "PROPERTY OWNER",
                            Safety_Equipment_Restraint = [],
                            State = "",
                            Unit_Number = str(unit_no),
                            Zip_Code = "")
            
            p_damage_driver_info = People(Address = "",
                            Address2 = "",
                            City = "",
                            Condition_At_Time_Of_Crash = [],
                            Contributing_Circumstances_Person = [],
                            Date_Of_Birth = "",
                            Driver_Distracted_By = [],
                            Drivers_License_Jurisdiction = "",
                            Drivers_License_Number = "",
                            Ejection = "",
                            First_Name = "",
                            Home_Phone = "",
                            Injury_Status = "",
                            Last_Name = "",
                            Middle_Name = "",
                            Name_Suffix = "",
                            Person_Type = "DRIVER",
                            Safety_Equipment_Restraint = [],
                            State = "",
                            Unit_Number = str(unit_no),
                            Zip_Code = "")    

            p_damage_vehicle_info = People(Address = name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['address'],
                                    Address2 = "",
                                    City = name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['city'],
                                    Condition_At_Time_Of_Crash = [],
                                    Contributing_Circumstances_Person = [],
                                    Date_Of_Birth = "",
                                    Driver_Distracted_By = [],
                                    Drivers_License_Jurisdiction = "",
                                    Drivers_License_Number = "",
                                    Ejection = "",
                                    First_Name = name_split_using_pattern(get_value_from_df(page_df,'Damage_Property_Owner_Name'),business_name_lookup_path=business_name_lookup_path)['first_name'],
                                    Home_Phone = "",
                                    Injury_Status = "",
                                    Last_Name = name_split_using_pattern(name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['last_name'],
                                    Middle_Name = name_split_using_pattern(name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['middle_name'],
                                    Name_Suffix = name_split_using_pattern(name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['names'],business_name_lookup_path=business_name_lookup_path)['suffix'],
                                    Person_Type = "VEHICLE OWNER",
                                    Safety_Equipment_Restraint = [],
                                    State = name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['state'],
                                    Unit_Number = str(unit_no),
                                    Zip_Code = name_address_split_using_lookup(get_value_from_df(page_df,'Property_Owner_Details'),city_lookup_path)['zipcode'],
                                    Same_as_Driver_GUI = "",
                                    Address_Same_as_Driver_GUI = "")



            vehicle_info = Vehicle(Air_Bag_Deployed = "",
                                  Contributing_Circumstances_Vehicle =  [],
                                  Damaged_Areas = [],
                                  Insurance_Company = "",
                                  Insurance_Expiration_Date = "",
                                  Insurance_Policy_Number = "",
                                  License_Plate =  "",
                                  Make = "",
                                  Make_Original = "" ,
                                  Model = "",
                                  Model_Original = "",
                                  Model_Year = "",
                                  Model_Year_Original = "",
                                  Posted_Statutory_SpeedLimit = "",
                                  Registration_State = "", 
                                  Road_Surface_Condition = [],
                                  Unit_Number = str(unit_no),
                                  VIN = "",
                                  VIN_Original  = "",
                                  Vehicle_Towed = "",
                                  VinValidation_VinStatus = "")
            
            if(len(p_damage_info.Last_Name) > 1 or len(p_damage_vehicle_info.Address) > 1 or len(p_damage_vehicle_info.First_Name) > 1 or len(p_damage_vehicle_info.Last_Name) > 1 or \
               len(p_damage_vehicle_info.State) > 1 or len(p_damage_vehicle_info.City) > 1):
                party_number += 1
                p_damage_info.Party_Id = str(party_number)
                p_damage_driver_info.Party_Id = str(party_number)
                p_damage_vehicle_info.Party_Id = str(party_number)
                
                people_list.append(p_damage_info)
                people_list.append(p_damage_driver_info)
                people_list.append(p_damage_vehicle_info)
                vehicle_list.append(vehicle_info)
    return people_list, vehicle_list, party_number

def citation_extraction(df, citation_list, citation_infos_list):
    try:

        unit_no = 0
        for dic in citation_infos_list:
            if("Citation_Code" in dic.keys() and "Citation_Unit_No" in dic.keys()):
                unit_no += 1
                cite_info = Citations(Violation_Code = get_violation_code(get_value_from_dict(dic, 'Citation_Code').strip()),
                                      Party_Id = str(unit_no),
                                      Unit_Number = str(unit_no))
                if(len(get_value_from_dict(dic, 'Citation_Code').strip()) > 1):
                    citation_list.append(cite_info)
        return citation_list
    except:
        return([])
    
def extract_json_arizona(text_extracted_df, infos_csv_path, city_lookup_path, business_name_lookup_path, mapping_df_path):
    global num_property_owner
    global party_number

    tif_name = text_extracted_df['path'].unique()[0].split("_")[0]+".tif"
    df = text_extracted_df
    df.fillna(' ', inplace=True)
    df_copy = df.copy()
    df.sort_values(by = ['path','ymin', 'xmin'], inplace = True, ascending = True)
    info_csv = pd.read_csv(infos_csv_path)
    mapping_df = pd.read_csv(mapping_df_path)
    
    driver_infos_list = list(info_csv['driver_info'].dropna().values)
    passenger_infos_list = list(info_csv['passenger_info'].dropna().values)
    citation_infos_list = list(info_csv['citation_info'].dropna().values)
    witness_infos_list = list(info_csv['witness_info'].dropna().values)
         
    driver_list, driver_count = get_driver_params(df,driver_infos_list)
    passenger_list, passenger_count = get_passenger_params(df_copy, passenger_infos_list)
    witness_list, all_witness_count  = get_witness_params(df, witness_infos_list)
    citation_infos_list, all_citation_count = get_citation_params(df, citation_infos_list)
    
    incident_list = []
    people_list = []
    vehicle_list = []
    citation_list = []
    
    incident_extracted_list = incident_extraction(df, incident_list, mapping_df)
    party_number = 0
    people_list = driver_extraction(df, driver_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df)
    people_list = vehicle_owner_extraction(df, driver_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df)
    vehicle_list = vehicle_detail_extraction(df, driver_list, vehicle_list, city_lookup_path, mapping_df)
    
    
    veh_unit = len(vehicle_list)
    total_party = len(people_list)
    
    vehicle_list, veh_unit = trailer_detail_extraction(df,driver_list,vehicle_list, city_lookup_path, mapping_df, veh_unit)
    num_property_owner = len(vehicle_list)
    
    people_list, vehicle_list, total_party = property_extraction(df, people_list, vehicle_list, passenger_list, city_lookup_path, \
                                                                 business_name_lookup_path, total_party, veh_unit)
    people_list, total_party = passenger_extraction(df, driver_list, passenger_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df, total_party)
    people_list = witness_extraction(df, driver_list, witness_list, people_list, city_lookup_path, business_name_lookup_path, mapping_df, total_party)   
    citation_list = citation_extraction(df, citation_list, citation_infos_list)
    
    report = Report(FormName = "Universal",
                    CountKeyed = "",
                    Incident = incident_extracted_list[0],
                    People = people_list,
                    Vehicles = vehicle_list,
                    Citations = citation_list)
    
    main_cls = MainCls(Report = report)
    
    
    data = json.dumps(main_cls, default = lambda o : o.__dict__, indent = 4)    
    return data, tif_name