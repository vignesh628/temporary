import numpy as np
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input as preprocess_input_vgg16
import cv2
import re



def detect_single_check_box(image, model, xmin = None, ymin = None, xmax = None, ymax = None):
    try:

        label_map = dict({0:"Checked",
                          1:"Unchecked"})
        
        if(xmin !=  None and xmax !=  None and ymin !=  None and ymax !=  None):
            xmin = int(xmin); ymin = int(ymin); xmax = int(xmax); ymax = int(ymax)
            image = image[ymin:ymax,xmin:xmax]

        image = img_to_array(image)

        image = cv2.resize(image, (224,224), interpolation = cv2.INTER_AREA)
        image = preprocess_input_vgg16(image)
        image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
        y_pred = model.predict(image)

        y_pred = np.argmax(y_pred, axis = 1)
        pred = label_map[y_pred[0]]
        del(model)
        
        if("Checked" in pred):
            return "Checked"
        else:
            return "Unchecked"

    except:
        return "Unchecked"
    
def get_damage_checked_bbox(image, xmin = None, ymin = None, xmax = None, ymax = None):
    try:
        if(xmin !=  None and xmax !=  None and ymin !=  None and ymax !=  None):
            xmin = int(xmin); ymin = int(ymin); xmax = int(xmax); ymax = int(ymax)
            image = image[ymin:ymax,xmin:xmax]        
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)            
            (thresh, im_bw) = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
            white = cv2.countNonZero(im_bw)
            black = im_bw.size - white
            fac = black/im_bw.size
        
            if(fac > 0.35):
                label = "Checked"
            else:
                label = "Unchecked"
    except:
        label = "Except"
    return label

def process_latitude(text):
    if(len(text) > 0):
        lat = re.sub('[^a-zA-Z0-9.]','',text).strip()
    else:
        lat = ""
    return lat


def process_longitude(text):
    if(len(text) > 0):
        long = re.sub('[^a-zA-Z0-9.]','',text).strip()
        long = "-" + long
    else:
        long = ""
    return long
