class ModelNotImplementedError(Exception):
    pass


class TensorflowServingError(Exception):
    pass
