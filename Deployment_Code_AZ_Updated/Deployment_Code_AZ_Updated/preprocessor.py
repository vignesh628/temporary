#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 12:03:43 2020

@author: saugatapaul
"""


import os
import subprocess
import PyPDF2
import glob
# from PIL import Image
import shutil
from tqdm import tqdm
import xml.etree.ElementTree as ET
import cv2
# from infer_detections import create_detection_df

def create_dirs(out_jpg_dir,out_xml_dir,form_type_path):

    if not os.path.exists(out_jpg_dir):
        os.makedirs(out_jpg_dir)
    if not os.path.exists(out_xml_dir):
        os.makedirs(out_xml_dir)
    if not os.path.exists(form_type_path[0]):
        os.makedirs(form_type_path[0])
    if not os.path.exists(form_type_path[1]):
        os.makedirs(form_type_path[1])

def tiff_to_jpg(tif_path):
    im=cv2.imreadmulti(tif_path)
    no_of_frames=len(im[1])
    input_img = os.path.basename(tif_path).split('.')[0]
    for i in range(no_of_frames):
        upd_i = i + 1
        im_name = input_img + "_" + str(upd_i) + ".jpg"
        im_name = os.path.join(os.path.dirname(tif_path),im_name)
        try:
            cv2.imwrite(im_name,im[1][i])
        except Exception as e:
            print(e)
# =============================================================================
# def tiff_to_jpg_backup(tif_path,out_jpg_dir):
#     img_list = glob.glob(tif_path)
#     for j in img_list:
#         im = Image.open(j)
#         no_of_frames = im.n_frames
#         input_img = os.path.basename(j).split('.')[0].split('_')[0]
#         im_path = os.path.join(out_jpg_dir,input_img)
#         if not os.path.exists(im_path):
#             os.mkdir(im_path)
#         for i in range(no_of_frames):
#             im.seek(i)
#             upd_i = i+1
#             im_name = input_img+"_"+str(upd_i)+".jpg"
#             im_name = os.path.join(im_path,im_name)
#             try:
#                 im1=im.convert("RGB")
#                 im1.save(im_name,"JPEG")
#             except Exception as e:
#                 print(e)
#                 im1=im.convert("RGB")
#                 im1.save(im_name,"JPEG")
# =============================================================================

def str_replace_xml(xml_str):
    xml_str=xml_str.replace('http://www.w3.org/1999/xhtml','')
    xml_str=xml_str.replace('❑','')
    xml_str=xml_str.replace('•','')
    xml_str=xml_str.replace('■','')
    return(xml_str)

def pdf_convert_text_linux(pdf_path, save_path, page_num):
    if os.path.exists(save_path) == False:
        os.chdir(r'/media/developer/Storage2/California/CA_Deployment/poppler-0.68.0/bin')
        argument = 'pdftotext.exe -bbox-layout' + ' -f ' + str(page_num) + ' -l ' + str(
            page_num) + ' ' + pdf_path + ' ' + save_path
        mysubproces = subprocess.Popen(argument, creationflags=subprocess.SW_HIDE, shell=True)
        mysubproces.wait()
        
def pdf_convert_text_windows(pdf_path, save_path, page_num):
    if os.path.exists(save_path) == False:
        os.chdir(r"D:\California_Deployment\poppler-0.68.0\bin")
        argument = 'pdftotext.exe -bbox-layout' + ' -f ' + str(page_num) + ' -l ' + str(page_num) + ' ' + pdf_path + ' ' + save_path
        #argument = argument.split()
        mysubproces = subprocess.Popen(argument)
        mysubproces.wait()
        
def extract_text(pdf_path, out_xml_dir):
    pdf_object = PyPDF2.PdfFileReader(pdf_path)
    page_number = pdf_object.getNumPages()
    ind_xml_path = out_xml_dir + os.path.basename(pdf_path).split('.')[0]
    if not os.path.exists(ind_xml_path):
        os.makedirs(ind_xml_path)
    for page_num in range(1, page_number + 1):
        # save_path = ind_xml_path + '/' + os.path.basename(pdf_path).split('.')[0] + '_' + str(page_num) + '.xml'
        save_path = os.path.join(ind_xml_path,os.path.basename(pdf_path).split('.')[0] + '_' + str(page_num) + '.xml')
        pdf_convert_text_linux(pdf_path, save_path, page_num)
        
        
def xml_convertion(pdf_path,out_xml_dir):
    pdf_list=os.listdir(pdf_path)
    for i in tqdm(pdf_list):
        path = os.path.join(pdf_path,i)
        extract_text(path,out_xml_dir)
        
def move_folder(out_jpg_dir,out_xml_dir,form_type_path,folder):
    form_jpg_path = os.path.join(form_type_path,folder,'jpg')
    form_xml_path = os.path.join(form_type_path,folder,'xml')
    shutil.move(os.path.join(out_jpg_dir,folder),form_jpg_path)
    shutil.move(os.path.join(out_xml_dir,folder),form_xml_path)


def identify_form_type(xml_tree,page_height,out_jpg_dir,out_xml_dir,form_type_path,folder):
    text_list=[]
    for elem in xml_tree.findall('.//page/flow/block/line/word'):
        if(float(elem.attrib['yMax'])<(float(page_height)/6)):
            text_list.append(elem.text)

    if (('CALIFORNIA' in text_list or 'STATE' in text_list) or ('CHP' in text_list or '555' in text_list) or
        ('PRIVATE' in text_list) or ('COUNTER' in text_list) or ('STATE' in text_list) or
        ('COUNTER REPORT' in text_list) or ('COLLISION' in text_list)):
        move_folder(out_jpg_dir,out_xml_dir,form_type_path[0],folder)
        print('Crash Form')
    else:
        move_folder(out_jpg_dir,out_xml_dir,form_type_path[1],folder)
        print('Non Crash Form')

def form_type_detection(out_jpg_dir,out_xml_dir,form_type_path):
    xml_folder_list=os.listdir(out_xml_dir)
    for folder in tqdm(xml_folder_list):
        xml_path = os.path.join(out_xml_dir,folder,'*.xml')
        xml_list = glob.glob(xml_path)
        jpg_path = os.path.join(out_jpg_dir,folder,'*.jpg')
        jpg_list = glob.glob(jpg_path)
        if(jpg_list==None):
            print("JPG corresponding to XML does not exist")
        else:
            try:
                xml_str = open(xml_list[0],'r',encoding="utf-8").read()
                upd_xml_str=str_replace_xml(xml_str)
                xml_tree=ET.XML(upd_xml_str)
                page_list=xml_tree.findall('.//page')
                page_height = page_list[0].attrib['height']
                identify_form_type(xml_tree,page_height,out_jpg_dir,out_xml_dir,form_type_path,folder)
            except Exception as e:
                print(e)
                
def rec_plot(coord, img, lab):
    xmin,ymin,xmax,ymax = coord
    ann_img=cv2.rectangle(img, (int(xmin),int(ymin)), (int(xmax),int(ymax)),(0,255,0),2)
    cv2.putText(ann_img, lab, (int(xmax-10), int(ymax+10)), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 0, 0), 2)
    return ann_img


# =============================================================================
# def draw_ann(df):
#     img_list = set(df.image_path)
#     n = len(img_list)
#     for j in tqdm(range(n)):
#         gt_df = df.sort_values(by='image_path')
#         gt_df = gt_df[~gt_df.classes.str.contains("classes")]
#         uniq_name = gt_df.image_path.unique()
#         image_path =str(uniq_name[j])
#         img = cv2.imread(image_path)
#         df_grp = gt_df.groupby("image_path").get_group(str(uniq_name[j]))
#         for i, row in df_grp.iterrows():
#             xmin = row.x1
#             ymin = row.y1
#             xmax = row.x2
#             ymax = row.y2
#             lab = row.classes
#             gt_ann_img = rec_plot((xmin,ymin,xmax,ymax), img, lab)
#         try:
#             os.remove(os.path.dirname(image_path)+"/"+(os.path.basename(image_path).split(".")[0])+"_annot.jpg")
#         except:
#             pass
#         ann_image= os.path.dirname(image_path)+"/"+(os.path.basename(image_path).split(".")[0])+"_annot.jpg"
#         cv2.imwrite(ann_image, gt_ann_img)
# =============================================================================

# =============================================================================
# def pred_detect(form_type_path, frozen_model_path, label_map):
#     form_type_list = os.listdir(form_type_path)
#     comp_img_list = []
#     
#     for j in form_type_list:
#         path = os.path.join(form_type_path,j,'jpg','*.jpg')
#         comp_img_list.extend(glob.glob(path))
# 
#     df=create_detection_df(comp_img_list, frozen_model_path, label_map, score_thresh=0.9)
#     df.to_csv(os.path.join(form_type_path,'predictions.csv'))
#     return(df) 
# =============================================================================
