#!/bin/bash
# TODO: Move this to Dockerfile
export ECRASH_LOG_DIR=/home/ecrash_user/ecrash_deployment/ecrash_log_dir
export PYTHONPATH=$PYTHONPATH:/home/ecrash_user/ecrash_deployment
gunicorn -t 1500 -w 3 --bind 0.0.0.0:8000 wsgi:app
